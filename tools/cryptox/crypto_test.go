package cryptox

import "testing"

func TestSalt(t *testing.T) {
	t.Run("test", func(t *testing.T) {
		s, err := Salt(32)
		if err != nil {
			t.Error(err)
		} else {
			t.Log(s)
		}
	})
}
