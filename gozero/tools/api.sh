#!/usr/bin/env bash
# go-zero api 生成脚本

# 使用方法：
# sh ./api.sh ${apiName}

goctl api go -api ./$1.api -dir ../ --style=gozero --home="/Users/zackeus/.goctl"