package store

import (
	"context"
	"time"
)

// ExpiredCallback cookie token 过期回调函数
// type ExpiredCallback func(token string)

// Store session 存储接口
type Store interface {
	// Delete 会话存储中删除会话令牌和相应的数据。如果令牌不存在或为空，则 Delete 应该是空操作并返回 nil（不是错误）
	Delete(uid string, tokens []string) (err error)

	// Clear 清除指定 uid 列表中会话令牌
	Clear(uids []string) (err error)

	// Get should return the data for a session token from the store. If the
	// session token is not found or is expired, the found return value should
	// be false (and the err return value should be nil). Similarly, tampered
	// or malformed tokens should result in a found return value of false and a
	// nil err value. The err return value should be used for system errors only.
	Get(token string) (b []byte, found bool, err error)

	// Find 根据唯一标识 uid 索引
	Find(uid string) (map[string][]byte, error)

	// Commit should add the session token and data to the store, with the given
	// expiry time. If the session token already exists, then the data and
	// expiry time should be overwritten.
	Commit(uid string, token string, b []byte, expiry time.Time) (err error)
}

// IterableStore is the interface for session stores which support iteration.
type IterableStore interface {
	// All should return a map containing data for all active sessions (i.e.
	// sessions which have not expired). The map key should be the session
	// token and the map value should be the session data. If no active
	// sessions exist this should return an empty (not nil) map.
	All() (map[string][]byte, error)
}

// CtxStore is an interface for session stores which take a context.Context
// parameter.
type CtxStore interface {
	Store

	// DeleteCtx is the same as Store.Delete, except it takes a context.Context.
	DeleteCtx(ctx context.Context, uid string, tokens []string) (err error)

	// ClearCtx 清除指定 uid 列表中会话令牌
	ClearCtx(ctx context.Context, uids []string) (err error)

	// GetCtx is the same as Store.Get, except it takes a context.Context.
	GetCtx(ctx context.Context, token string) (b []byte, found bool, err error)

	// FindCtx is the same as Store.Find, except it takes a context.Context.
	FindCtx(ctx context.Context, uid string) (map[string][]byte, error)

	// CommitCtx is the same as Store.Commit, except it takes a context.Context.
	CommitCtx(ctx context.Context, uid string, token string, b []byte, expiry time.Time) (err error)
}

// IterableCtxStore is the interface for session stores which support iteration
// and which take a context.Context parameter.
type IterableCtxStore interface {
	// AllCtx is the same as IterableStore.All, expect it takes a
	// context.Context.
	AllCtx(ctx context.Context) (map[string][]byte, error)
}
