package sip

import "fmt"

// MaybeString String wrapper
type MaybeString interface {
	String() string
	Equals(other any) bool
}

type String struct {
	Str string
}

func (str String) String() string {
	return str.Str
}

func (str String) Equals(other any) bool {
	if v, ok := other.(string); ok {
		return str.Str == v
	}
	if v, ok := other.(String); ok {
		return str.Str == v.Str
	}

	return false
}

// Port number
type Port uint16

func (port *Port) Clone() *Port {
	if port == nil {
		return nil
	}
	newPort := *port
	return &newPort
}

func (port *Port) String() string {
	if port == nil {
		return ""
	}
	return fmt.Sprintf("%d", *port)
}

func (port *Port) Equals(other any) bool {
	/* 如果两个指针都不为 nil，则检查底层 uint16 的相等性 */
	/* 如果任一指针为 nil，当且仅当它们都为 nil 时才返回 true */
	if port == nil || other == nil {
		return port == other
	}
	if p, ok := other.(*Port); ok {
		return *port == *p
	}
	return false
}
