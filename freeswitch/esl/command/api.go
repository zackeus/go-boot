package command

import (
	"fmt"
	"gitee.com/zackeus/goutil/strutil"
	"github.com/google/uuid"
)

type API struct {
	Command    string
	Arguments  string
	Background bool
	JobUUID    string
}

func (api *API) BuildCmd() string {
	if api.Background {
		return fmt.Sprintf("bgapi %s", api.Command)
	}
	return fmt.Sprintf("api %s", api.Command)
}

func (api *API) BuildMessage() string {
	if api.Background {
		if strutil.IsBlank(api.JobUUID) {
			api.JobUUID = uuid.New().String()
		}
		return fmt.Sprintf("bgapi %s %s \r\nJob-UUID: %s", api.Command, api.Arguments, api.JobUUID)
	}
	return fmt.Sprintf("api %s %s", api.Command, api.Arguments)
}
