#!/usr/bin/env bash
# go-zero rpc 生成脚本(根据项目结构调整 DIR)

# 使用方法：
# sh ./rpc.sh ${serviceName}

PROJECT="cti"
NAME="ippbx"

# MOD goland mod 目录
MOD="/Users/zackeus/golang/gopath/pkg/mod"
# 工作路径 /Users/zackeus/golang/workspaces
WORK_DIR=$(cd "$(dirname "$0")" || return;cd ../../../; pwd)
# rpc 路径
RPC_DIR="$WORK_DIR/$PROJECT/$NAME"


# rpc gateway
_=$(mkdir -p "$RPC_DIR/pb")
_=$(protoc --descriptor_set_out="$RPC_DIR"/pb/$NAME.pb $NAME.proto)
# rpc server
# -m: 多 service 构建
_=$(goctl rpc protoc "$RPC_DIR"/protobuf/$NAME.proto --go_out="$RPC_DIR" --go-grpc_out="$RPC_DIR" --zrpc_out="$RPC_DIR" -I "$WORK_DIR" -I "$MOD" -style gozero -m --home="/Users/zackeus/.goctl")
# 自定义 tag
protoc-go-inject-tag -input="$RPC_DIR"/proto/*.pb.go
