package websocket

import (
	"gitee.com/zackeus/go-boot/common/constants/net/headers"
	message2 "gitee.com/zackeus/go-boot/websocket/message"
	"gitee.com/zackeus/go-zero/core/logx"
	"gitee.com/zackeus/go-zero/core/service"
	"gitee.com/zackeus/go-zero/rest"
	"net/http"
	"testing"
)

var (
	port    = 3333
	timeout = 0
	cpu     = 500
)

var ws *Engine

func OnConnected(client *Client, isForce bool) {
	logx.Infof("OnConnected: %s %v", client.ID(), isForce)
}

func OnDisConnected(client *Client, isForce bool) {
	logx.Infof("OnDisConnected: %s %v", client.ID(), isForce)
}

func OnMessage(client *Client, msg *message2.Message) {
	logx.Infof("OnMessage: %s %s", client.ID(), msg.Value)

	if msg.Value == "close" {
		_ = client.Close(&message2.CloseMessage{Code: message2.CloseForceUnRegister, Value: "Force UnRegister."})
	}
}

func OnError(client *Client, err error) {
	logx.Errorf("OnError: %s %s", client.ID(), err)
}

func TestWebsocket(t *testing.T) {
	logx.DisableStat()
	engine := rest.MustNewServer(rest.RestConf{
		ServiceConf: service.ServiceConf{
			Log: logx.LogConf{
				Mode: "console",
			},
		},
		Host:         "localhost",
		Port:         port,
		Timeout:      int64(timeout),
		CpuThreshold: int64(cpu),
	})
	defer engine.Stop()

	ws, err := NewEngine(WithCheckOrigin(func(r *http.Request) bool {
		return true
	}))
	if err != nil {
		logx.Error(err)
		return
	}

	engine.AddRoute(rest.Route{
		Method: http.MethodGet,
		Path:   "/",
		Handler: func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path != "/" {
				http.Error(w, "Not found", http.StatusNotFound)
				return
			}
			if r.Method != "GET" {
				http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
				return
			}

			http.ServeFile(w, r, "ws.html")
		},
	})

	// 添加路由
	engine.AddRoute(rest.Route{
		Method: http.MethodGet,
		Path:   "/ws",
		Handler: func(w http.ResponseWriter, r *http.Request) {
			/* 将http请求升级为websocket */
			client, err := ws.ServeWs(w, r)
			if err != nil {
				return
			}

			/* 注册 */
			ws.Register(r.Header.Get(headers.SecWebSocketKey), client,
				WithOnMessage(OnMessage),
				WithOnConnected(OnConnected),
				WithOnDisConnected(OnDisConnected),
				WithOnError(OnError),
			)
		},
	})

	engine.Start()
}
