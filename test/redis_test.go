package test

import (
	"gitee.com/zackeus/go-boot/freeswitch/mod/xmlcurl/dialplan"
	"gitee.com/zackeus/go-zero/core/stores/redis"
	"github.com/beevik/etree"
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestRedisXMLGet(t *testing.T) {
	client, err := redis.NewRedis(redis.RedisConf{
		Host: "redis.server:6379",
		Pass: "yulon123",
		Type: "node",
	})
	if err != nil {
		t.Log(err)
		return
	}

	s, err := client.Get("test")
	if err != nil {
		t.Log(err)
		return
	}

	doc := etree.NewDocument()
	if err := doc.ReadFromString(s); err != nil {
		t.Error(err)
	}

	doc.Indent(1)
	_, _ = doc.WriteTo(os.Stdout)
}

func TestRedisXMLSet(t *testing.T) {
	xml, err := dialplan.New("test", dialplan.Type("outbound"))
	if err != nil {
		t.Log(err)
	}

	client, err := redis.NewRedis(redis.RedisConf{
		Host: "redis.server:6379",
		Pass: "yulon123",
		Type: "node",
	})
	if err != nil {
		t.Log(err)
		return
	}

	b, err := xml.MarshalToXml()
	if err != nil {
		t.Log(err)
		return
	}

	if err := client.Set("test", string(b)); err != nil {
		t.Log(err)
		return
	}
}

func TestRedisIncr(t *testing.T) {
	client, err := redis.NewRedis(redis.RedisConf{
		Host: "192.168.137.32:6379,192.168.137.33:6379,192.168.137.34:6379",
		Pass: "yulon123",
		Type: "cluster",
	})
	assert.Empty(t, err)

	key := "test:key"
	id, err := client.Incr(key)
	assert.Empty(t, err)
	t.Log("id: ", id)

	_ = client.Expire(key, 30)
}

func TestRedisCluster(t *testing.T) {
	client, err := redis.NewRedis(redis.RedisConf{
		Host: "192.168.137.32:6379,192.168.137.33:6379,192.168.137.34:6379,192.168.137.32:6380,192.168.137.33:6380,192.168.137.34:6380",
		Pass: "yulon123",
		Type: "cluster",
	})
	assert.Empty(t, err)

	id, err := client.Incr("freeswitch:event:seq")
	assert.Empty(t, err)
	t.Log("id: ", id)
}
