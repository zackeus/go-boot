package zh

import (
	"gitee.com/zackeus/go-boot/validator/tags"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	translations "github.com/go-playground/validator/v10/translations/zh"
)

func RegisterDefaultTranslations(v *validator.Validate, trans ut.Translator) error {
	if err := translations.RegisterDefaultTranslations(v, trans); err != nil {
		return err
	}

	if err := v.RegisterTranslation(tags.TagFieldLen, trans, func(ut ut.Translator) error {
		return ut.Add(tags.TagFieldLen, "{0}长度必须是{1}个字符", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(tags.TagFieldLen, fe.Field(), fe.Param())
		return t
	}); err != nil {
		return err
	}

	if err := v.RegisterTranslation(tags.TagChoiceSingleDigit, trans, func(ut ut.Translator) error {
		return ut.Add(tags.TagChoiceSingleDigit, "{0}必须在指定范围内", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(tags.TagChoiceSingleDigit, fe.Field())
		return t
	}); err != nil {
		return err
	}

	if err := v.RegisterTranslation(tags.TagDtmf, trans, func(ut ut.Translator) error {
		return ut.Add(tags.TagDtmf, "{0}无效的 DTMF 按键", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(tags.TagDtmf, fe.Field())
		return t
	}); err != nil {
		return err
	}
	return nil
}
