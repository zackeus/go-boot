package directory

import (
	"os"
	"testing"
)

func TestNew(t *testing.T) {
	x, err := New("5101", "admin123", "cti.yulon-finance.com.cn", "private")
	if err != nil {
		t.Error(err)
		return
	}

	if p, ok := x.(*Xml); ok {
		p.doc.Indent(1)
		_, _ = p.doc.WriteTo(os.Stdout)
	}
}
