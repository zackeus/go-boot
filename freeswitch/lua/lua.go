package lua

type Script string

const (
	// LocalScript 本地执行脚本
	LocalScript Script = "localScript.lua"
	// LocalCall 本地分机呼叫
	LocalCall Script = "localCall.lua"
	// GatewayCall 外线呼叫
	GatewayCall Script = "gatewayCall.lua"
	// AlegPreOriginate aleg 收到媒体之后执行
	AlegPreOriginate Script = "alegPreOriginate.lua"
	// AlegPreBridge aleg 桥接前处理
	AlegPreBridge Script = "alegPreBridge.lua"
	// BlegAfterAnswer bleg 应答后执行
	BlegAfterAnswer Script = "blegAfterAnswer.lua"
	// HookExtHangup 分机挂断执行
	HookExtHangup Script = "hookExtHangup.lua"
	// HookExtRing 分机振铃执行
	HookExtRing Script = "hookExtRing.lua"
	// HandleDialerDst 被叫处理
	HandleDialerDst Script = "handleDialerDst.lua"
	// LocalIvr 本地IVR
	LocalIvr Script = "localIvr.lua"
	// IvrPreNode ivr节点预处理
	IvrPreNode Script = "ivrPreNode.lua"
	// IvrAfterNode ivr节点后处理
	IvrAfterNode Script = "ivrAfterNode.lua"
	// LocalCallcenter 本地呼叫中心
	LocalCallcenter Script = "localCallcenter.lua"
	// LocalThreeWay 本地三方通话
	LocalThreeWay Script = "localThreeWay.lua"
)
