package stringx

import (
	"fmt"
	"strconv"
	"sync"
	"testing"
)

var wg sync.WaitGroup

func fTest(i int, f string, args map[string]any) {
	s := Format(f, args)
	fmt.Println(fmt.Sprintf("the num: %v, the format: %s, the string: %s", i, f, s))
	wg.Done()
}

func TestFormat(t *testing.T) {
	f := make([]string, 0)
	n := make([]int, 0)
	for i := 0; i < 30; i++ {
		f = append(f, fmt.Sprintf("hi this is {{.%v}}", i))
		n = append(n, i)
	}

	for i, s := range f {
		wg.Add(1)
		zz := strconv.FormatInt(int64(i), 10)
		go fTest(i, s, map[string]any{zz: "haha " + zz})
	}

	wg.Wait() //阻塞直到所有任务完成
	t.Log("over********")
}
