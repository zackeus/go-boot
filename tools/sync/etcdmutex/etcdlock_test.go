package etcdmutex

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-zero/core/logx"
	"go.etcd.io/etcd/client/pkg/v3/transport"
	clientv3 "go.etcd.io/etcd/client/v3"
	"log"
	"sync"
	"testing"
	"time"
)

// etcd 分布式锁测试
func BenchmarkEtcdLock(b *testing.B) {
	var sg sync.WaitGroup
	sg.Add(1)

	/* 证书 */
	tlsInfo := transport.TLSInfo{
		TrustedCAFile: "/Users/zackeus/golang/workspaces/go-zero-demo/certs.d/etcd/ca.pem",
		CertFile:      "/Users/zackeus/golang/workspaces/go-zero-demo/certs.d/etcd/client-cert.pem",
		KeyFile:       "/Users/zackeus/golang/workspaces/go-zero-demo/certs.d/etcd/client-key.pem",
	}
	tlsConfig, err := tlsInfo.ClientConfig()
	if err != nil {
		fmt.Printf("tlsconfig failed, err:%v\n", err)
	}

	/* 连接实例化 */
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   []string{"etcd.node1.server:2379"},
		TLS:         tlsConfig,
		DialTimeout: 5 * time.Second,
	})
	if err != nil {
		log.Fatal(err)
	}
	defer cli.Close()

	mum := New(cli, WithPrefix("mall/"))

	// 会话1上锁成功，然后开启goroutine去新建一个会话去上锁，5秒钟后会话1解锁。
	l1, err := mum.Acquire(context.Background(), WithKey("test"), WithTTL(5))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("session1 上锁成功。 time：%d \n", time.Now().Unix())

	go func() {
		defer sg.Done()

		rootContext := context.Background()
		/* 创建 3 秒超时的ctx */
		ctx, cancelFunc := context.WithTimeout(rootContext, 20*time.Second)
		defer cancelFunc()

		l2, err := mum.Acquire(ctx, WithKey("test"), WithTTL(20))
		if err != nil {
			fmt.Println("session2 上锁失败")
			logx.Error(err)
			return
		}
		fmt.Printf("session2 上锁成功。 time：%d \n", time.Now().Unix())
		if err := l2.Release(rootContext); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("session2 解锁。 time：%d \n", time.Now().Unix())

	}()

	time.Sleep(10 * time.Second)
	if err := l1.Release(context.TODO()); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("session1 解锁。 time：%d \n", time.Now().Unix())

	sg.Wait()
}
