package events

const (
	// SpeechBegin 开始说话
	SpeechBegin = "begin-speaking"
	// SpeechDetected 检测到语音
	SpeechDetected = "detected-speech"
	// SpeechClosed 关闭检测
	SpeechClosed = "closed"
)
