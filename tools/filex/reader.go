package filex

import (
	"bufio"
	"io"
	"os"
)

const (
	defaultBufSize = 8192
)

// ReadPath 流式读取文件
// path: 文件路径
// fn: 文件流回调
// size: 单次读取文件大小
func ReadPath(path string, fn func(content []byte) error, size ...uint64) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer func(file *os.File) {
		_ = file.Close()
	}(file)

	/* 默认 1024 * 1024 */
	size = append(size, 1024*1024)
	return ReadStream(file, fn, size...)
}

// ReadStream 流式读取文件
// rd: Reader 接口
// fn: 文件流回调
// size: 单次读取文件大小
func ReadStream(rd io.Reader, fn func(content []byte) error, size ...uint64) error {
	/* 创建 Reader 自定义缓冲区大小 */
	reader := bufio.NewReaderSize(rd, defaultBufSize)

	size = append(size, defaultBufSize)
	buffer := make([]byte, size[0])
	for {
		n, err := reader.Read(buffer)
		if err != nil && err != io.EOF {
			return err
		}

		/* 最后一次读取可能不是 buffer 块大小 所以需要用 n 取块大小 */
		if err := fn(buffer[:n]); err != nil {
			return err
		}
		/* 读取结束 */
		if err == io.EOF {
			break
		}
	}
	return nil
}
