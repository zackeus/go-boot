package profile

import (
	"os"
	"testing"
)

func TestNewSipProfile(t *testing.T) {
	x, err := New("test", "5060", "internal",
		WithIp("127.0.0.1"),
		WithIp("127.0.0.5"),
	)
	if err != nil {
		t.Error(err)
		return
	}

	if p, ok := x.(*Xml); ok {
		p.doc.Indent(1)
		_, _ = p.doc.WriteTo(os.Stdout)
	}
}
