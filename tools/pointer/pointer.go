package pointer

import (
	"time"
)

// Int returns a pointer to an int
func Int(i int) *int {
	return &i
}

// Int32 returns a pointer to an int32.
func Int32(i int32) *int32 {
	return &i
}

// Uint returns a pointer to an uint
func Uint(i uint) *uint {
	return &i
}

// Uint32 returns a pointer to an uint32.
func Uint32(i uint32) *uint32 {
	return &i
}

// Int64 returns a pointer to an int64.
func Int64(i int64) *int64 {
	return &i
}

// Uint64 returns a pointer to an uint64.
func Uint64(i uint64) *uint64 {
	return &i
}

// Bool returns a pointer to a bool.
func Bool(b bool) *bool {
	return &b
}

// String returns a pointer to a string.
func String(s string) *string {
	return &s
}

// Float32 returns a pointer to a float32.
func Float32(i float32) *float32 {
	return &i
}

// Float64 returns a pointer to a float64.
func Float64(i float64) *float64 {
	return &i
}

// Duration returns a pointer to a time.Duration.
func Duration(d time.Duration) *time.Duration {
	return &d
}
