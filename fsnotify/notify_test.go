package fsnotify

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"os/signal"
	"syscall"
	"testing"
)

func onEvent(event Event) {
	fmt.Println("event2: ", event)
}

func TestNew(t *testing.T) {
	watcher, err := New(WithIgnoreHiddenFile(false), WithEventConcurrentSize(10))
	assert.Empty(t, err)
	defer watcher.Shutdown()

	_, err = watcher.Watch("/Users/zackeus/mnt/log/cdr_error", onEvent)
	assert.Empty(t, err)

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGUSR1, syscall.SIGUSR2, syscall.SIGTERM, syscall.SIGINT)
	<-signals
}
