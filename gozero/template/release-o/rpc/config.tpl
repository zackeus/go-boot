package config

import "gitee.com/zackeus/go-zero/zrpc"

type Config struct {
	zrpc.RpcServerConf
}
