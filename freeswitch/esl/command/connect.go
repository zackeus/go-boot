package command

type Connect struct{}

func (Connect) BuildCMD() string {
	return "connect"
}

func (Connect) BuildMessage() string {
	return "connect"
}
