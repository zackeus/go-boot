package main

import (
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-boot/freeswitch/esl/events"
	"gitee.com/zackeus/go-zero/core/logx"
	"os"
)

func onReconnected() {
	logx.Info("onReconnected *************************")
}

func onEvent(event *events.Event) {
	fmt.Println(event)
}

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.71:8021", esl.WithOnReconnected(onReconnected))
	sig := make(chan os.Signal)

	// Register an event listener for all events
	listenerID1 := inbound.Subscribe(events.EventChannelCallState, onEvent)
	listenerID2 := inbound.Subscribe(events.EventSofiaRegister, onEvent)
	listenerID3 := inbound.Subscribe(events.EventSofiaUnRegister, onEvent)
	//listenerID5 := inbound.Subscribe(events.EventCallCenter, onEvent)
	//listenerID6 := inbound.Subscribe(events.EventIvrLog, onEvent)

	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	<-sig
	// Remove the listener and close the connection gracefully
	inbound.UnSubscribe(events.EventBackRoundJob, listenerID1, listenerID2, listenerID3)
	inbound.Close()
}
