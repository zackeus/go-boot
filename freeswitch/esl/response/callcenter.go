package response

import "strconv"

type Queue struct {
	Name                   string `json:"name"`
	Strategy               string `json:"strategy"`
	TimeBaseScore          string `json:"time_base_score"`
	AgentNoAnswerStatus    string `json:"agent_no_answer_status"`
	AbandonedResumeAllowed bool   `json:"abandoned_resume_allowed"`
	CallsAbandoned         uint64 `json:"calls_abandoned"`
	CallsAnswered          uint64 `json:"calls_answered"`
}

func (q *Queue) Unmarshal(data map[string]string) error {
	q.Name = data["name"]
	q.Strategy = data["strategy"]
	q.TimeBaseScore = data["time_base_score"]
	q.AgentNoAnswerStatus = data["time_base_score"]

	if v, err := strconv.ParseBool(data["abandoned_resume_allowed"]); err != nil {
		q.AbandonedResumeAllowed = v
		return err
	}
	if v, err := strconv.ParseUint(data["calls_abandoned"], 10, 64); err != nil {
		q.CallsAbandoned = v
		return err
	}
	if v, err := strconv.ParseUint(data["calls_answered"], 10, 64); err != nil {
		q.CallsAnswered = v
		return err
	}
	return nil
}
