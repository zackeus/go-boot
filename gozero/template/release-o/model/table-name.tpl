func (m *default{{.upperStartCamelObject}}Model) tableName() string {
	return m.table
}

func (m *default{{.upperStartCamelObject}}Model) buildFlavor() sqlbuilder.Flavor {
	return sqlbuilder.MySQL
}

func (m *default{{.upperStartCamelObject}}Model) cacheKeys(k ...string) []string {
	keys := make([]string, 0, len(bizCacheKeys)+len(k))
	keys = append(keys, bizCacheKeys...)
	keys = append(keys, k...)
	return keys
}
