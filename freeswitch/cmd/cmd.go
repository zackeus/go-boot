package cmd

import "gitee.com/zackeus/go-boot/tools/jsonx"

type (
	Method string

	CMD struct {
		FileName    string `json:"fileName"`              // xml 文件名
		Method      Method `json:"method"`                // 操作方式 增删改查
		Md5         string `json:"md5,omitempty"`         // xml 文件 md5 值
		ProfileName string `json:"profileName,omitempty"` // profile 名称
		GatewayName string `json:"gatewayName,omitempty"` // gateway 名称
		QueueName   string `json:"queueName,omitempty"`   // callcenter queue 名称
	}
)

const (
	MethodAdd    Method = "add"
	MethodUpdate Method = "update"
	MethodDelete Method = "delete"

	ReloadXml     string = "reloadxml"
	ReloadAcl     string = "reloadacl"
	ReloadProfile string = "reloadprofile"
	ReloadGw      string = "reloadgw"
	ReloadQueue   string = "reloadqueue"
)

func (c *CMD) MarshalToJson() (string, error) {
	return jsonx.MarshalToString(c)
}

func UnMarshalFromString(str string) (*CMD, error) {
	c := &CMD{}
	err := jsonx.UnmarshalFromString(str, c)
	return c, err
}
