package command

import (
	"fmt"
	"gitee.com/zackeus/goutil/strutil"
)

type Auth struct {
	User     string
	Password string
}

func (auth Auth) BuildCmd() string {
	if strutil.IsNotBlank(auth.User) {
		return "userauth"
	}
	return "auth"
}

func (auth Auth) BuildMessage() string {
	if strutil.IsNotBlank(auth.User) {
		return fmt.Sprintf("userauth %s:%s", auth.User, auth.Password)
	}
	return fmt.Sprintf("auth %s", auth.Password)
}
