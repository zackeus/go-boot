package message

import (
	"github.com/gorilla/websocket"
)

type (
	CloseMessage struct {
		Code  CloseCode
		Value string
	}

	Message struct {
		Type  Type
		Value string
	}
)

// Format 格式化
func (m *CloseMessage) Format() []byte {
	return websocket.FormatCloseMessage(int(m.Code), m.Value)
}
