// 监听
// 监听建立后，可按以下功能键切换：
// 1：与被监听一方单独通话，另外一方听背景音(耳语)
// 2: 与被监听的对端单独通话，被监听的分机放背景音
// 3: 转三方通话
// 0: 恢复监听模式

package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-boot/freeswitch/lua"
	"gitee.com/zackeus/go-zero/core/logx"
	"strconv"
	"time"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.71:8021")
	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	defer inbound.Close()

	ctx := context.Background()

	timeout := 15
	// 需要监听的UUID
	spyUuid := "c457c960-3713-479a-8bc3-31b4f39aa1f9"

	aLeg := esl.Leg{CallURL: "user/1003", LegVariables: map[string]string{
		freeswitch.VarCallType:                  string(freeswitch.CallSpy),
		freeswitch.VarEavesdropUuid:             spyUuid,
		freeswitch.VarLegTimeout:                strconv.FormatInt(int64(timeout), 10),
		freeswitch.VarOriginationCallerIdName:   "裕隆",
		freeswitch.VarOriginationCallerIdNumber: "500100",
		freeswitch.VarIgnoreEarlyMedia:          "false",
		freeswitch.VarExecuteOnAnswer:           fmt.Sprintf("lua %s", lua.BlegAfterAnswer),
	}}
	bLeg := esl.Leg{
		CallURL: fmt.Sprintf("&eavesdrop(%s)", spyUuid),
	}
	if uuid, err := inbound.OriginateCallSync(ctx, aLeg, bLeg, map[string]string{}, time.Duration(timeout)*time.Second); err != nil {
		logx.Error(err)
	} else {
		logx.Error("ok: ", uuid)
	}
}
