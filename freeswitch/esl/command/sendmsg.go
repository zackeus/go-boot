package command

import (
	"fmt"
	"gitee.com/zackeus/goutil/strutil"
	"strings"
)

type SendMessage struct {
	UUID    string
	Command string
	AppName string
	AppArg  string
	Sync    bool
}

func (s *SendMessage) BuildCmd() string {
	return "sendmsg"
}

func (s *SendMessage) BuildMessage() string {
	var cmd strings.Builder
	cmd.WriteString(fmt.Sprintf("sendmsg %s\r\n", s.UUID))

	if s.Sync {
		// Waits for this event to finish before continuing even in async mode
		cmd.WriteString("event-lock: true\r\n")
		// No documentation on this flag, I assume it takes priority over the other flag?
		cmd.WriteString("event-lock-pri: true\r\n")
	}
	cmd.WriteString(fmt.Sprintf("call-command: %s\r\n", s.Command))
	if strutil.IsNotBlank(s.AppName) {
		cmd.WriteString(fmt.Sprintf("execute-app-name: %s\r\n", s.AppName))
		if strutil.IsNotBlank(s.AppArg) {
			cmd.WriteString(fmt.Sprintf("execute-app-arg: %s\r\n", s.AppArg))
		}
	}
	// Remove the extra \r\n
	return cmd.String()[:cmd.Len()-2]
}
