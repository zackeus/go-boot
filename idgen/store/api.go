package store

type (
	IdStore interface {
		Available() bool
		GetWorkerId() uint16 // 获取 workId
		Shutdown(g bool)
	}
)
