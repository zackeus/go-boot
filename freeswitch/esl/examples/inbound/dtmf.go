package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-zero/core/logx"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.71:8021")
	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	defer inbound.Close()

	ctx := context.Background()

	key := "*"
	uuid := "9e9d4d90-80b5-40a2-91c8-a7b3d4bd5561"

	/* 发送DTMF */
	//if err := inbound.SendDTMF(ctx, uuid, key); err != nil {
	//	logx.Error(err)
	//}
	/* 接收DTMF */
	if err := inbound.RecvDTMF(ctx, uuid, key); err != nil {
		logx.Error(err)
	}
}
