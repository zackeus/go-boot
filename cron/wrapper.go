package cron

import "gitee.com/zackeus/go-boot/cron/core"

// StableJob 此类型将在重新启动服务的节点中恢复
type StableJob interface {
	core.Job
	GetCron() string
	Serialize() ([]byte, error)
	UnSerialize([]byte) error
}

// JobWrapper is a job wrapper
type JobWrapper struct {
	ID      core.EntryID
	Cron    *Cron
	Name    string
	CronStr string
	Job     core.Job
}

// Run is run job
func (job JobWrapper) Run() {
	/* 如果该任务分配给了这个节点 则允许执行 */
	// TODO 使用回掉函数替代
	if job.Cron.allowThisNodeRun(job.Name) {
		job.Execute()
	}
}

func (job JobWrapper) Execute() {
	if job.Job != nil {
		job.Job.Run()
	}
}
