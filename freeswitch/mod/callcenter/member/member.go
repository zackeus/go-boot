package member

type (
	// State 队列成员状态
	State string
)

const (
	// StateUnknown 未知状态
	StateUnknown State = "Unknown"
	// StateWaiting 等待分配坐席
	StateWaiting State = "Waiting"
	// StateTrying 等待坐席接听(坐席端振铃)
	StateTrying State = "Trying"
	// StateAnswered 通话中
	StateAnswered State = "Answered"
	// StateAbandoned 服务结束
	StateAbandoned State = "Abandoned"
)
