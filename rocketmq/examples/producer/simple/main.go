package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/rocketmq"
	"gitee.com/zackeus/go-boot/rocketmq/primitive"
	"gitee.com/zackeus/go-boot/rocketmq/producer"
	"gitee.com/zackeus/go-boot/rocketmq/rlog"
	"gitee.com/zackeus/go-zero/core/logx"
	"gitee.com/zackeus/goutil"
	"os"
)

const (
	Topic     = "cti-event-topic"
	Endpoint  = "rmq.nameserver1.server:9876"
	AccessKey = "testuser"
	SecretKey = "yulon123"
)

// Package main implements a simple producer to send message.
func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}
	rlog.SetLevel("info")

	nameservers := []string{Endpoint}
	/* 消息追踪 */
	traceCfg := &primitive.TraceConfig{
		Access:   primitive.Local,
		Resolver: primitive.NewPassthroughResolver(nameservers),
	}

	p, _ := rocketmq.NewProducer(
		producer.WithNsResolver(primitive.NewPassthroughResolver(nameservers)),
		producer.WithTrace(traceCfg),
		/* 鉴权消息 */
		producer.WithCredentials(primitive.Credentials{
			AccessKey: AccessKey,
			SecretKey: SecretKey,
		}),
		/* 生产组 */
		producer.WithGroupName("test"),
	)
	if err := p.Start(); err != nil {
		fmt.Printf("start producer error: %s", err.Error())
		os.Exit(1)
	}

	//for i := 0; i < 5; i++ {
	//	msg := &primitive.Message{
	//		Topic: Topic,
	//		Body:  []byte("Hello RocketMQ Go Client! " + strconv.Itoa(i)),
	//	}
	//	res, err := p.SendSync(context.Background(), msg)
	//
	//	if err != nil {
	//		fmt.Printf("send message error: %s\n", err)
	//	} else {
	//		fmt.Printf("send message success: result=%s\n", res.String())
	//	}
	//}

	msg := &primitive.Message{
		Topic: Topic,
		Body:  []byte("Hello RocketMQ Go Client! "),
	}
	msg.WithTag("test")
	msg.WithProperty("111", "123")
	_, err := p.SendSync(context.Background(), msg)
	goutil.PanicErr(err)

	if err := p.Shutdown(); err != nil {
		fmt.Printf("shutdown producer error: %s", err.Error())
	}

}
