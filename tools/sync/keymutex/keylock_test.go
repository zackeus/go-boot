package keymutex

import (
	"fmt"
	"math/rand"
	"sync"
	"testing"
	"time"
)

func TestNewKeyLock(t *testing.T) {
	var keyLock *KeyMutex
	keyLock = NewKeyMutex()

	keyLock.Lock("shi")
	defer keyLock.Unlock("shi")

	//TODO 需要处理的流程
}

func TestKeyLock(t *testing.T) {
	var wg sync.WaitGroup
	keyLock := NewKeyMutex()
	wg.Add(5)
	for i := 0; i < 5; i++ {
		go func(i int) {
			keyLock.Lock(fmt.Sprintf("%v", i))
			fmt.Println("do test", fmt.Sprintf("%v", i))
			time.Sleep(1 * time.Second)
			defer keyLock.Unlock(fmt.Sprintf("%v", i))

		}(1)
		wg.Done()
	}
	wg.Wait()
}

// go test keylock_test.go -bench .
func BenchmarkKeyLock(b *testing.B) {
	keyLock := NewKeyMutex()

	var keys []string
	for n := 0; n < 100000; n++ {
		keys = append(keys, fmt.Sprintf("key%d", n))
	}
	// 测试一个对象或者函数在多线程的场景下面是否安全
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			RunKeyLock(keyLock, keys)
		}
	})
}

func BenchmarkLock(b *testing.B) {
	mu := &sync.Mutex{}

	var keys []string
	for n := 0; n < 100000; n++ {
		keys = append(keys, fmt.Sprintf("key%d", n))
	}
	// 测试一个对象或者函数在多线程的场景下面是否安全
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			RunLock(mu, keys)
		}
	})
}

func RunKeyLock(keyLock *KeyMutex, keys []string) {
	index := rand.Intn(10)
	key := keys[index]
	fmt.Println(key, "locking")
	/* 奇数使用key锁 */
	keyLock.Lock(key)
	fmt.Println(key, "locked")
	keyLock.Unlock(key)
	fmt.Println(key, "unlock")
}

func RunLock(mu *sync.Mutex, keys []string) {
	index := rand.Intn(10)
	key := keys[index]
	fmt.Println(key, "locking")
	mu.Lock()
	fmt.Println(key, "locked")
	mu.Unlock()
	fmt.Println(key, "unlock")
}
