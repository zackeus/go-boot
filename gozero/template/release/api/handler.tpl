package {{.PkgName}}

import (
	"net/http"
	"gitee.com/zackeus/go-boot/common/constants/codes"

	"gitee.com/zackeus/go-boot/tools/httpx/response"
	{{if .HasRequest}}"gitee.com/zackeus/go-zero/rest/httpx"{{end}}
	{{.ImportPackages}}
)

{{if .HasDoc}}{{.Doc}}{{end}}
func {{.HandlerName}}(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		{{if .HasRequest}}var req types.{{.RequestType}}
		if err := httpx.Parse(r, &req); err != nil {
            /* 参数异常 */
            response.WriteJsonCtx(r.Context(), w, http.StatusUnprocessableEntity, &response.Response{
                Code: codes.UnprocessableEntity,
                Msg:  err.Error(),
            })
			return
		}

		{{end}}l, err := {{.LogicName}}.New{{.LogicType}}(r.Context(), svcCtx)
           if err != nil {
               response.ErrorJson(r.Context(), w, err)
               return
           }
           /* 权限校验 */
           if err := l.ValidatePermissions(); err != nil {
               response.WriteJsonCtx(r.Context(), w, http.StatusForbidden, &response.Response{
                   Code: codes.Forbidden,
                   Msg:  err.Error(),
               })
               return
           }

		{{if .HasResp}}resp, {{end}}err := l.{{.Call}}({{if .HasRequest}}&req{{end}})
		if err != nil {
		    /* 异常响应 */
		    response.ErrorJson(r.Context(), w, err)
		} else {
			{{if .HasResp}}response.OkJson(r.Context(), w, resp){{else}}response.Ok(w){{end}}
		}
	}
}
