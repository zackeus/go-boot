package message

import "github.com/gorilla/websocket"

type (
	// CloseCode  关闭代码 1000-2999 协议预留 3000-3999 自定义
	CloseCode int
)

const (
	// CloseNormalClosure 正常关闭连接
	CloseNormalClosure CloseCode = websocket.CloseNormalClosure
	// CloseGoingAway going away 终端离开, 可能因为服务端错误, 也可能因为浏览器正从打开连接的页面跳转离开
	CloseGoingAway CloseCode = websocket.CloseGoingAway
	// CloseProtocolError 终端由于协议错误终止了连接
	CloseProtocolError CloseCode = websocket.CloseProtocolError
	// CloseUnsupportedData 由于接收到不允许的数据类型而断开连接 (如仅接收文本数据的终端接收到了二进制数据)
	CloseUnsupportedData CloseCode = websocket.CloseUnsupportedData
	// CloseNoStatusReceived 终端期望收到状态码但是没有收到
	CloseNoStatusReceived CloseCode = websocket.CloseNoStatusReceived
	// CloseAbnormalClosure 连接被异常关闭
	CloseAbnormalClosure CloseCode = websocket.CloseAbnormalClosure
	// CloseInvalidFramePayloadData 数据内容和实际的消息类型不匹配
	CloseInvalidFramePayloadData CloseCode = websocket.CloseInvalidFramePayloadData
	// ClosePolicyViolation 由于收到不符合约定的数据而断开连接. 这是一个通用状态码, 用于不适合使用 1003 和 1009 状态码的场景
	ClosePolicyViolation CloseCode = websocket.ClosePolicyViolation
	CloseMessageTooBig   CloseCode = websocket.CloseMessageTooBig
	// CloseMandatoryExtension 在握手阶段，客户端希望服务器使用多个扩展，但是服务器没有返回相应的扩展信息
	CloseMandatoryExtension CloseCode = websocket.CloseMandatoryExtension
	// CloseInternalServerErr 服务内部错误
	CloseInternalServerErr CloseCode = websocket.CloseInternalServerErr
	CloseServiceRestart    CloseCode = websocket.CloseServiceRestart
	CloseTryAgainLater     CloseCode = websocket.CloseTryAgainLater
	// CloseTLSHandshake 不能进行TLS握手
	CloseTLSHandshake CloseCode = websocket.CloseTLSHandshake

	// CloseForceUnRegister 强制注销
	CloseForceUnRegister CloseCode = 3000
	// CloseUnauthorized 未鉴权
	CloseUnauthorized CloseCode = 3401
)
