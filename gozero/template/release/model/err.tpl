package {{.pkg}}

import "gitee.com/zackeus/go-zero/core/stores/sqlx"

var ErrNotFound = sqlx.ErrNotFound
