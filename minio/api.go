package minio

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"gitee.com/zackeus/go-boot/tools/errorx"
	"gitee.com/zackeus/go-zero/core/logx"
	"github.com/go-resty/resty/v2"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"golang.org/x/sync/singleflight"
	"net/http"
	"sync"
	"time"
)

const (
	// 节点活跃度 测试MinIO服务器是否在线
	healthLiveURl = "minio/health/live"
	// 集群写入仲裁检测
	healthClusterURL = "minio/health/cluster"
	// 集群读取仲裁检测
	healthClusterReadURL = "minio/health/cluster/read"
)

type (
	Client struct {
		*minio.Client
		once        sync.Once
		g           singleflight.Group
		restyClient *resty.Client
		cancel      context.CancelFunc
	}

	Options struct {
		AccessKey    string
		AccessSecret string
		Token        string
		Secure       bool
		TlsConfig    *tls.Config
	}
)

func New(endpoint string, opts *Options) (*Client, error) {
	if opts == nil {
		return nil, errors.New("no options provided")
	}

	baseUrl := fmt.Sprintf("http://%s", endpoint)
	if opts.Secure {
		baseUrl = fmt.Sprintf("https://%s", endpoint)
	}

	/* Create a Resty Client */
	restyCli := resty.New()
	restyCli.SetBaseURL(baseUrl)
	restyCli.SetTLSClientConfig(opts.TlsConfig)
	/* 超时时长 */
	restyCli.SetTimeout(3 * time.Second)

	minioCli, err := minio.New(endpoint, &minio.Options{
		Creds:     credentials.NewStaticV4(opts.AccessKey, opts.AccessSecret, opts.Token),
		Secure:    opts.Secure,
		Transport: &http.Transport{TLSClientConfig: opts.TlsConfig},
	})
	if err != nil {
		return nil, err
	}

	/* 启用健康检查 周期3s */
	cancel, err := minioCli.HealthCheck(time.Second * 3)
	if err != nil {
		return nil, err
	}

	return &Client{Client: minioCli, restyClient: restyCli, cancel: cancel}, nil
}

// Available 服务是否可用
func (client *Client) Available() bool {
	health, _, _ := client.g.Do("health", func() (any, error) {
		if client.Client == nil || client.Client.IsOffline() {
			return false, nil
		}

		/* 使用健康检测api */
		resp, err := client.restyClient.R().Get(healthClusterURL)
		if err != nil {
			logx.Error(errorx.Wrap(err, "minio cluster health check err."))
			return false, nil
		}

		return resp.IsSuccess(), nil
	})

	return health.(bool)
}

func (client *Client) Close() {
	client.once.Do(func() {
		if client.cancel != nil {
			client.cancel()
			client.cancel = nil
		}
	})
}
