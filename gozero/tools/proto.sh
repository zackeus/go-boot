#!/usr/bin/env bash
# protobuf 生成脚本(根据项目结构调整 DIR)
# --validate_out protoc-gen-validate 校验插件(go get github.com/envoyproxy/protoc-gen-validate)
# protoc-go-inject-tag 自定义tag插件(go get github.com/favadi/protoc-go-inject-tag)

PROJECT="cti"
NAME="ipcc"

MOD="/Users/zackeus/golang/gopath/pkg/mod"
# 工作路径 /Users/zackeus/golang/workspaces
WORK_DIR=$(cd "$(dirname "$0")" || return;cd ../../../../; pwd)
# rpc 路径
RPC_DIR="$WORK_DIR/$PROJECT/$NAME/rpc"

# protoc --go_out="$DIR" --go-grpc_out="$DIR" -I . -I "$MOD" --validate_out="lang=go:$DIR" ./types/$1.proto
# 等待 proto 生成
_=$(protoc --go_out="$RPC_DIR" --go-grpc_out="$RPC_DIR" -I "$WORK_DIR" -I "$MOD" "$RPC_DIR"/protobuf/types/$1.proto)
# 自定义 tag
protoc-go-inject-tag -input="$RPC_DIR"/proto/*.pb.go