package codec

import (
	"time"
)

// Codec session 编解码接口
type Codec interface {
	Encode(uid string, deadline time.Time, values map[string]interface{}) ([]byte, error)
	Decode([]byte) (uid string, deadline time.Time, values map[string]interface{}, err error)
}
