package xmlcurl

// mod_xml_curl configuration 请求 key_value
const (
	// KeyValueAcl ACL 访问控制
	KeyValueAcl = "acl.conf"
	// KeyValueIvr IVR 菜单
	KeyValueIvr = "ivr.conf"
	// KeyValueCallCenter callcenter
	KeyValueCallCenter = "callcenter.conf"

	// ActionSipAuth mod_sofia 使用它来执行身份验证检查
	ActionSipAuth = "sip_auth"
	// ActionMessageCount mod_voicemail使用来检查消息是否正在等待 - 需要响应示例
	ActionMessageCount      = "message-count"
	ActionReverseAuthLookup = "reverse-auth-lookup"
)
