package queue

type (
	// Score 队列分数
	Score string
	// Strategy 分配策略
	Strategy string
)

const (
	// ScoreQueue 任何来电的基础评分在进入队列之前不会被增加（无论他们的呼叫的总长度）
	ScoreQueue Score = "queue"
	// ScoreSystem 将增加原来已经通话过(或进入系统)的来电电话基础评分。提高来电评分让在队列中等待时间较长的得到接听.只要在系统中的来电优先于其它队列中的来电
	ScoreSystem Score = "system"
)

const (
	// StrategyRingAll 同时呼叫所有的客户端，某个接听后其它客户端自动挂断
	StrategyRingAll Strategy = "ring-all"
	// StrategyLeastTalkTime 最少通话时间的客户端优先
	StrategyLeastTalkTime Strategy = "agent-with-least-talk-time"
	// StrategyFewestCalls 最少通话次数的客户端优先
	StrategyFewestCalls Strategy = "agent-with-fewest-calls"
	// StrategyLongestIdle 最大空闲客户端优先
	StrategyLongestIdle Strategy = "longest-idle-agent"
	// StrategyProgressively 渐进(按照tiers.level和tiers.position排序坐席。每 ring_progressively_delay 秒，增加分配一个坐席)
	StrategyProgressively Strategy = "ring-progressively"
	// StrategyRoundRobin 最后一次客户端通话的优先
	StrategyRoundRobin Strategy = "round-robin"
	// StrategyAgentOrder 按照规则和等级依次查找客户端(Level: 值越小等级越高  Position: 值越小地位就越高)
	StrategyAgentOrder Strategy = "sequentially-by-agent-order"
	// StrategyTopDown 从上倒下查找客户端
	StrategyTopDown Strategy = "top-down"
	// StrategyRandom 随机
	StrategyRandom Strategy = "random"
	// StrategyRandomSqlserver 随机(sqlserver)
	StrategyRandomSqlserver Strategy = "random_sqlserver"
	// StrategyRandomMysql 随机(mysql)
	StrategyRandomMysql Strategy = "random_mysql"
)
