package tags

const (
	// TagFieldLen 校验 field 长度 eg: validate:"fieldlen=1"`
	TagFieldLen = "fieldlen"
	// TagChoiceSingleDigit 校验 field 在个位数选择范围 eg: validate:"choiceSingle"`
	TagChoiceSingleDigit = "choiceSingle"
	// TagDtmf 校验 dtmf 按键
	TagDtmf = "dtmf"
)
