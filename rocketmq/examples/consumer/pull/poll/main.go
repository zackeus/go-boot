/*
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	rocketmq "gitee.com/zackeus/go-boot/rocketmq"
	"gitee.com/zackeus/go-zero/core/logx"
	"gitee.com/zackeus/goutil"
	_ "net/http/pprof"
	"time"

	"gitee.com/zackeus/go-boot/rocketmq/rlog"

	"gitee.com/zackeus/go-boot/rocketmq/consumer"
	"gitee.com/zackeus/go-boot/rocketmq/primitive"
)

const (
	Endpoint      = "rmq-dev.nameserver1.server:9876"
	Topic         = "test-topic"
	ConsumerGroup = "test-group"

	accessKey = "testuser"
	secretKey = "baicfc123"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}
	rlog.SetLevel("error")

	pullConsumer, err := rocketmq.NewPullConsumer(
		consumer.WithGroupName(ConsumerGroup),
		consumer.WithNsResolver(primitive.NewPassthroughResolver([]string{Endpoint})),
		consumer.WithCredentials(primitive.Credentials{
			AccessKey: accessKey,
			SecretKey: secretKey,
		}),
		consumer.WithMaxReconsumeTimes(3),
		consumer.WithConsumeMessageBatchMaxSize(20),
	)
	goutil.PanicErr(err)
	defer pullConsumer.Shutdown()

	err = pullConsumer.Subscribe(Topic, consumer.MessageSelector{})
	goutil.PanicErr(err)
	err = pullConsumer.Start()
	goutil.PanicErr(err)

	ctx := context.TODO()
	for {
		cr, err := pullConsumer.Poll(ctx, time.Second*3)
		if consumer.IsNoNewMsgError(err) {
			logx.Info("无数据****************************")
			continue
		}
		goutil.PanicErr(err)

		for _, msg := range cr.GetMsgList() {
			data := string(msg.Body)
			logx.Info(msg.GetProperty(primitive.PropertyRetryTopic), " : ", data)
			logx.Info(msg)
			pullConsumer.ACK(ctx, cr, consumer.ConsumeSuccess)
		}
	}
}
