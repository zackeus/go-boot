package stringx

import (
	"gitee.com/zackeus/goutil/strutil"
	"unicode"
)

// IsDigit 判断字符串是否为十进制数字
func IsDigit(s string) bool {
	if strutil.IsBlank(s) {
		return true
	}

	for _, c := range []rune(s) {
		if !unicode.IsDigit(c) {
			return false
		}
	}
	return true
}
