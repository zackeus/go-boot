package gateway

import (
	"os"
	"testing"
)

func TestNewGateway(t *testing.T) {
	x, err := New("test", "127.0.0.1", "udp",
		WithRegister(false),
		WithFromUser("5100"),
		WithVariable("area-code", "120000"),
	)
	if err != nil {
		t.Error(err)
		return
	}

	if p, ok := x.(*Xml); ok {
		p.doc.Indent(1)
		_, _ = p.doc.WriteTo(os.Stdout)
	}
}
