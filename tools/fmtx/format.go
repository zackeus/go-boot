package fmtx

import "gitee.com/zackeus/goutil/comdef"

// MapToSlice map 转 slice
// filter: 过滤函数(返回 true 会放入切片中 否则忽略)
func MapToSlice[K comdef.SimpleType, T any](m map[K]T, filter func(i any) bool) ([]K, []T) {
	sK := make([]K, 0, len(m))
	sT := make([]T, 0, len(m))

	for k, t := range m {
		if filter(k) {
			sK = append(sK, k)
		}
		if filter(t) {
			sT = append(sT, t)
		}
	}
	return sK, sT
}
