#!/usr/bin/env bash
# docker 远程构建脚本

# env
# "DOCKER_HOST": "tcp://docker.build.test.server:2376",
# "DOCKER_CERT_PATH": "/Users/zackeus/server-dev/docker-build",
# "DOCKER_TLS_VERIFY": "1"

docker build --pull --rm -f "Dockerfile" -t ${tag} .