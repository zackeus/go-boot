package dialplan

import (
	"os"
	"testing"
)

func TestNewDialplan(t *testing.T) {
	x, err := New("test", "outbound")
	if err != nil {
		t.Error(err)
		return
	}

	if p, ok := x.(*Xml); ok {
		p.doc.Indent(1)
		_, _ = p.doc.WriteTo(os.Stdout)
		p.doc.WriteToFile("/Users/zackeus/Downloads/test.xml")
	}
}
