package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-boot/freeswitch/lua"
	"gitee.com/zackeus/go-zero/core/logx"
	"strconv"
	"time"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.71:8021")
	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	defer inbound.Close()

	ctx := context.Background()

	timeout := 15
	// 三方发起者分机号
	src := "1000"
	// 需要转三方的UUID(三方发起者)
	srcUuid := "359f5049-9c45-41b6-91e7-d25246ac45d8"
	aLeg := esl.Leg{CallURL: "user/1003", LegVariables: map[string]string{
		freeswitch.VarThreewayUuid:              srcUuid,
		freeswitch.VarLegTimeout:                strconv.FormatInt(int64(timeout), 10),
		freeswitch.VarOriginationCallerIdName:   "裕隆",
		freeswitch.VarOriginationCallerIdNumber: "500100",
		freeswitch.VarIgnoreEarlyMedia:          "false",
	}}
	bLeg := esl.Leg{CallURL: fmt.Sprintf("&lua('%s %s %s')", lua.LocalThreeWay, src, srcUuid)}
	if uuid, err := inbound.OriginateCallSync(ctx, aLeg, bLeg, map[string]string{}, time.Duration(timeout)*time.Second); err != nil {
		logx.Error(err)
	} else {
		logx.Error("ok: ", uuid)
	}
}
