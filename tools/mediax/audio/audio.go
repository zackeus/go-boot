package audio

import (
	"gitee.com/zackeus/goutil/fsutil"
	"github.com/tidwall/gjson"
	ffmpeggo "github.com/u2takey/ffmpeg-go"
)

// Audio 音频文件信息
type Audio struct {
	FileName       string  // 文件名
	CodecType      string  // 编码类型
	CodecName      string  // 编码名称
	CodecLongName  string  // 编码全名
	FormatName     string  // 格式名称
	FormatLongName string  // 格式全名
	SampleRate     uint64  // 采样率
	Channels       uint64  // 声道数
	Duration       float64 // 总时长
	Size           uint64  // 字节大小
}

// Probe 音频文件解析
func Probe(path string) (*Audio, error) {
	s, err := ffmpeggo.Probe(path)
	if err != nil {
		return nil, err
	}

	/* json 解析 */
	r := gjson.Parse(s)
	stream := r.Get("streams").Array()[0]
	format := r.Get("format")

	return &Audio{
		FileName:      fsutil.Name(format.Get("filename").String()),
		CodecType:     stream.Get("codec_type").String(),
		CodecName:     stream.Get("codec_name").String(),
		CodecLongName: stream.Get("codec_long_name").String(),
		FormatName:    format.Get("format_name").String(),
		SampleRate:    stream.Get("sample_rate").Uint(),
		Channels:      stream.Get("channels").Uint(),
		Duration:      format.Get("duration").Float(),
		Size:          format.Get("size").Uint(),
	}, nil
}
