// 注销分机
package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-zero/core/logx"
)

func initESL() (*esl.InboundEngine, error) {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		return nil, err
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.58:8021")
	if err := inbound.Start(); err != nil {
		return nil, err
	}

	return inbound, nil
}

func main() {
	// Connect to FreeSWITCH
	inbound, err := initESL()
	if err != nil {
		fmt.Println(err)
		return
	}
	defer inbound.Close()

	profile := "internal"
	callId := "c54668-0-13c4-6790dd80-67f155cd-6790dd80"
	if err := inbound.Logout(context.Background(), profile, callId); err != nil {
		logx.Error(err)
	}
}
