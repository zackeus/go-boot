// freeswitch global vars 变量 key

package freeswitch

const (
	// VarSwitchInstance switch 实例ID
	VarSwitchInstance string = "switch-instance"

	// VarIgnoreEarlyMedia 忽略早期媒体
	VarIgnoreEarlyMedia string = "ignore_early_media"

	// VarOriginationUuid 设置 originate uuid
	VarOriginationUuid string = "origination_uuid"

	// VarOriginateTimeout 作用于originate 区间
	VarOriginateTimeout string = "originate_timeout"
	// VarCallTimeout A leg 设置 作用于 B leg
	VarCallTimeout string = "call_timeout"
	// VarLegTimeout 分别作用于当前设置的leg上
	VarLegTimeout string = "leg_timeout"

	// VarOriginationCallerIdName 来电名称(作用于originate 区间)
	VarOriginationCallerIdName string = "origination_caller_id_name"
	// VarOriginationCallerIdNumber 来电号码(作用于originate 区间)
	// originate user/1000 &bridge({origination_caller_id_number=88888}user/1001)
	VarOriginationCallerIdNumber string = "origination_caller_id_number"
	// VarEffectiveCallerIdName 来电名称(A leg 设置 作用于 B leg)
	VarEffectiveCallerIdName string = "effective_caller_id_name"
	// VarEffectiveCallerIdNumber 来电号码(A leg 设置 作用于 B leg)
	// b-leg的参数来源于a-leg，b-leg的主叫号码来源于a-leg的effective_caller_id_number
	// originate {effective_caller_id_number=888888}user/1000 &bridge(user/1001)
	VarEffectiveCallerIdNumber string = "effective_caller_id_number"

	// VarExecuteOnAnswer 在 answer 后执行(作用于 当前 leg)
	VarExecuteOnAnswer string = "execute_on_answer"

	// ------------------------------ 自定义 ------------------------------

	// VarCallType 呼叫类型
	VarCallType string = "call_type"
	// VarThreewayUuid 三方发起方UUID
	VarThreewayUuid = "threeway_uuid"
	// VarEavesdropUuid 窃听的UUID
	VarEavesdropUuid = "eavesdrop_uuid"
	// VarBusinessAccessCode 业务接入码
	VarBusinessAccessCode string = "business_access_code"
	// VarEnableSpeechAsr 开启语音转写
	VarEnableSpeechAsr string = "enable_speech_asr"
	// VarCcSide 呼叫中心方式(originator: 发起者,最终话单会记录呼叫分机绑定的坐席)
	VarCcSide string = "cc_side"
)
