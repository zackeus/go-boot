package freeswitch

var (
	hangupCauses map[string]HangupCause
)

func init() {
	hangupCauses = make(map[string]HangupCause, 0)
	hangupCauses["UNSPECIFIED"] = HangupUnspecified
	hangupCauses["CHANNEL_UNACCEPTABLE"] = HangupChannelUnacceptable
	hangupCauses["CALL_AWARDED_DELIVERED"] = HangupCallAwardedDelivered
	hangupCauses["NORMAL_CLEARING"] = HangupNormalClearing
	hangupCauses["RESPONSE_TO_STATUS_ENQUIRY"] = HangupResponseToStatusEnquiry
	hangupCauses["ACCESS_INFO_DISCARDED"] = HangupAccessInfoDiscarded
	hangupCauses["PRE_EMPTED"] = HangupPreEmpted
	hangupCauses["FACILITY_NOT_SUBSCRIBED"] = HangupFacilityNotSubscribed
	hangupCauses["CHAN_NOT_IMPLEMENTED"] = HangupChanNotImplemented
	hangupCauses["INVALID_CALL_REFERENCE"] = HangupInvalidCallReference
	hangupCauses["INVALID_MSG_UNSPECIFIED"] = HangupInvalidMsgUnspecified
	hangupCauses["MANDATORY_IE_MISSING"] = HangupMandatoryIeMissing
	hangupCauses["MESSAGE_TYPE_NONEXIST"] = HangupMessageTypeNonexist
	hangupCauses["WRONG_MESSAGE"] = HangupWrongMessage
	hangupCauses["IE_NONEXIST"] = HangupIeNonexist
	hangupCauses["INVALID_IE_CONTENTS"] = HangupInvalidIeContents
	hangupCauses["WRONG_CALL_STATE"] = HangupWrongCallState
	hangupCauses["MANDATORY_IE_LENGTH_ERROR"] = HangupMandatoryIeLengthError
	hangupCauses["PROTOCOL_ERROR"] = HangupProtocolError
	hangupCauses["INTERWORKING"] = HangupInterWorking
	hangupCauses["CRASH"] = HangupCrash
	hangupCauses["SYSTEM_SHUTDOWN"] = HangupSystemShutdown
	hangupCauses["LOSE_RACE"] = HangupLoseRace
	hangupCauses["MANAGER_REQUEST"] = HangupManagerRequest
	hangupCauses["BLIND_TRANSFER"] = HangupBlindTransfer
	hangupCauses["ATTENDED_TRANSFER"] = HangupAttendedTransfer
	hangupCauses["ALLOTTED_TIMEOUT"] = HangupAllottedTimeout
	hangupCauses["USER_CHALLENGE"] = HangupUserChallenge
	hangupCauses["MEDIA_TIMEOUT"] = HangupMediaTimeout
	hangupCauses["PICKED_OFF"] = HangupPickedOff
	hangupCauses["USER_NOT_REGISTERED"] = HangupUserNotRegistered
	hangupCauses["PROGRESS_TIMEOUT"] = HangupProgressTimeout
	hangupCauses["GATEWAY_DOWN"] = HangupGatewayDown
	hangupCauses["INVALID_GATEWAY"] = HangupInvalidGateway
	hangupCauses["UNALLOCATED_NUMBER"] = HangupUnallocatedNumber
	hangupCauses["NO_ROUTE_TRANSIT_NET"] = HangupNoRouteTransitNet
	hangupCauses["NO_ROUTE_DESTINATION"] = HangupNoRouteDestination
	hangupCauses["NO_USER_RESPONSE"] = HangupNoUserResponse
	hangupCauses["NUMBER_CHANGED"] = HangupNumberChanged
	hangupCauses["REDIRECTION_TO_NEW_DESTINATION"] = HangupRedirectionToNewDestination
	hangupCauses["NORMAL_UNSPECIFIED"] = HangupNormalUnspecified
	hangupCauses["NO_ANSWER"] = HangupNoAnswer
	hangupCauses["SUBSCRIBER_ABSENT"] = HangupSubscriberAbsent
	hangupCauses["EXCHANGE_ROUTING_ERROR"] = HangupExchangeRoutingError
	hangupCauses["INVALID_NUMBER_FORMAT"] = HangupInvalidNumberFormat
	hangupCauses["USER_BUSY"] = HangupUserBusy
	hangupCauses["ORIGINATOR_CANCEL"] = HangupOriginatorCancel
	hangupCauses["BEARERCAPABILITY_NOTIMPL"] = HangupBearercapabilityNotimpl
	hangupCauses["INCOMPATIBLE_DESTINATION"] = HangupIncompatibleDestination
	hangupCauses["FACILITY_REJECTED"] = HangupFacilityRejected
	hangupCauses["FACILITY_NOT_IMPLEMENTED"] = HangupFacilityNotImplemented
	hangupCauses["SERVICE_NOT_IMPLEMENTED"] = HangupServiceNotImplemented
	hangupCauses["NETWORK_OUT_OF_ORDER"] = HangupNetworkOutOfOrder
	hangupCauses["DESTINATION_OUT_OF_ORDER"] = HangupDestinationOutOfOrder
	hangupCauses["NORMAL_CIRCUIT_CONGESTION"] = HangupNormalCircuitCongestion
	hangupCauses["NORMAL_TEMPORARY_FAILURE"] = HangupNormalTemporaryFailure
	hangupCauses["SWITCH_CONGESTION"] = HangupSwitchCongestion
	hangupCauses["REQUESTED_CHAN_UNAVAIL"] = HangupRequestedChanUnavail
	hangupCauses["BEARERCAPABILITY_NOTAVAIL"] = HangupBearercapabilityNotavail
	hangupCauses["RECOVERY_ON_TIMER_EXPIRE"] = HangupRecoveryOnTimerExpire
	hangupCauses["CALL_REJECTED"] = HangupCallRejected
}

// IsValidHangupCause 验证 cause 是否有效
func IsValidHangupCause(cause string) bool {
	_, ok := hangupCauses[cause]
	return ok
}

// IsSipUserReachable 根据 PingStatus 验证sip分机是否可用
func IsSipUserReachable(status string) bool {
	return status == "Reachable"
}

// IsRecordDiscarded 根据 cause 判断录音是否被丢弃
func IsRecordDiscarded(cause string) bool {
	switch RecordCompletionCause(cause) {
	case CompletionCauseSuccessSilence, CompletionCauseNoInputTimeout, CompletionCauseSuccessMaxTime:
		return false
	default:
		return true
	}
}
