package arrx

import (
	"encoding/json"
	"testing"
)

type MenuNodes []*Menu

// Len 实现 sort 接口 Len方法返回集合中的元素个数
func (n MenuNodes) Len() int {
	return len(n)
}

// Less 实现 sort 接口 Less 判断索引为 i 的元素是否必须排在索引为 j 的元素之前
// 如果 Less(i, j) 和 Less(j, i) 都为假，则索引 i 和 j 处的元素被视为相等
func (n MenuNodes) Less(i, j int) bool {
	return n[i].Sort < n[j].Sort
}

// Swap 实现 sort 接口 Swap 元素交换
func (n MenuNodes) Swap(i, j int) {
	n[i], n[j] = n[j], n[i]
}

func (n MenuNodes) Lists() []*Menu {
	return n
}

type Menu struct {
	Id       string  `json:"id"`
	ParentId string  `json:"parent_id"`
	Name     string  `json:"name"`
	Sort     uint64  `json:"sort"`
	Children []*Menu `json:"children"`
}

func (m *Menu) ID() string {
	return m.Id
}

func (m *Menu) PID() string {
	return m.ParentId
}

func (m *Menu) Childs() []*Menu {
	return m.Children
}

func (m *Menu) SetChilds(n []*Menu) {
	m.Children = n
}

// 列表转树
func TestToTree(t *testing.T) {
	menus := []*Menu{
		{Id: "0", ParentId: "", Name: "Menu root", Sort: 10},
		{Id: "1", ParentId: "0", Name: "Menu 1", Sort: 2},
		{Id: "2", ParentId: "0", Name: "Menu 2", Sort: 1},
		{Id: "3", ParentId: "1", Name: "Submenu 1", Sort: 3},
		{Id: "4", ParentId: "1", Name: "Submenu 2", Sort: 4},
		{Id: "5", ParentId: "3", Name: "Subsubmenu 1", Sort: 1},
	}
	nodes := (MenuNodes)(menus)

	t.Run("test-tree", func(t *testing.T) {
		lists := ToTree[*Menu, MenuNodes](nodes, "0")

		bytes, err := json.Marshal(lists)
		if err != nil {
			t.Error(err)
			return
		}
		t.Log(string(bytes))
	})
}
