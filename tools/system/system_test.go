package system

import "testing"

func TestPasswdEncode(t *testing.T) {
	type args struct {
		salt string
		pwd  string
	}
	tests := []struct {
		name string
		args args
	}{
		{name: "TestPasswdEncode", args: args{salt: "O8tY9aHmhk0Ggia+1kgiDX2MefBj5GsQ", pwd: "admin"}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := PasswdEncode(tt.args.salt, tt.args.pwd)
			if err != nil {
				t.Error(err)
				return
			} else {
				t.Log(s)
			}
		})
	}
}

func TestVerifyPasswd(t *testing.T) {
	type args struct {
		p    string
		pwd  string
		salt string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "TestVerifyPasswd", args: args{p: "2fc771c4f82e570d9e9d3f98682b863a5b21f7cb52d7ad85d868d7851565defe", pwd: "admin", salt: "O8tY9aHmhk0Ggia+1kgiDX2MefBj5GsQ"}, want: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := VerifyPasswd(tt.args.p, tt.args.pwd, tt.args.salt); got != tt.want {
				t.Errorf("VerifyPasswd() = %v, want %v", got, tt.want)
			}
		})
	}
}
