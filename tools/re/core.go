package re

import "regexp"

var regexpUUID *regexp.Regexp

func init() {
	regexpUUID = regexp.MustCompile(string(PatternUUID))
}

// UUID 提取 UUID 字符
func UUID(s string) []string {
	return regexpUUID.FindStringSubmatch(s)
}

// Search 根据正则提取字符
// r: 正则表达式
// s: 待提取源字符
func Search(r *regexp.Regexp, s string) []string {
	return r.FindStringSubmatch(s)
}
