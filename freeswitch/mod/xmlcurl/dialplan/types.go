package dialplan

type Type string

const (
	// Inbound 呼入路由
	Inbound Type = "inbound"
	// Outbound 呼出路由
	Outbound Type = "outbound"
	// Other 其他
	Other Type = "other"
)
