package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/rocketmq"
	"gitee.com/zackeus/go-boot/rocketmq/primitive"
	"gitee.com/zackeus/go-boot/rocketmq/producer"
	"gitee.com/zackeus/go-boot/rocketmq/rlog"
	"gitee.com/zackeus/go-zero/core/logx"
	"math/rand"
	"os"
	"strconv"
	"time"
)

const (
	Topic     = "test-topic"
	Endpoint  = "192.168.137.26:9876"
	AccessKey = "testuser"
	SecretKey = "yulon123"
)

func random(min, max int) int64 {
	rand.Seed(time.Now().UnixNano())
	return int64(rand.Intn(max-min) + 1)
}

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}
	rlog.SetLevel("error")

	nameservers := []string{Endpoint}
	/* 消息追踪 */
	traceCfg := &primitive.TraceConfig{
		Access:   primitive.Local,
		Resolver: primitive.NewPassthroughResolver(nameservers),
	}

	p, _ := rocketmq.NewProducer(
		producer.WithNsResolver(primitive.NewPassthroughResolver(nameservers)),
		producer.WithTrace(traceCfg),
		/* 鉴权消息 */
		producer.WithCredentials(primitive.Credentials{
			AccessKey: AccessKey,
			SecretKey: SecretKey,
		}),
		/* 队列选择器 根据 sharding key hash 分配 */
		producer.WithQueueSelector(producer.NewHashQueueSelector()))
	if err := p.Start(); err != nil {
		fmt.Printf("start producer error: %s", err.Error())
		os.Exit(1)
	}

	for i := 0; i < 15; i++ {
		id := strconv.FormatInt(random(1, 5), 10)
		msg := &primitive.Message{
			Topic: Topic,
			Body:  []byte("Hello RocketMQ Go Client! " + strconv.Itoa(i)),
		}
		msg.WithTag(id)
		msg.WithKey(id)
		fmt.Printf(" will send message : %s\n", id)
		/* ShardingKey 保证 queue 有序 */
		msg.WithShardingKey(id)

		res, err := p.SendSync(context.Background(), msg)

		if err != nil {
			fmt.Printf("send message error: %s\n", err)
		} else {
			fmt.Printf("send message success: result=%s\n", res.String())
		}
	}

	if err := p.Shutdown(); err != nil {
		fmt.Printf("shutdown producer error: %s", err.Error())
	}
}
