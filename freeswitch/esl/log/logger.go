package log

import (
	"fmt"
	"gitee.com/zackeus/go-zero/core/logx"
	"os"
	"strings"
)

var (
	defaultLogger struct {
		level uint32
	}
)

const (
	prefix string = "[FS ESL] "
)

func init() {
	defaultLogger = struct{ level uint32 }{level: logx.DebugLevel}
	SetLevel(os.Getenv("ESL_LOG_LEVEL"))
}

func SetLevel(level string) {
	if level == "" {
		return
	}

	switch strings.ToLower(level) {
	case "info", "warn":
		defaultLogger.level = logx.InfoLevel
	case "error", "fatal":
		defaultLogger.level = logx.ErrorLevel
	case "debug":
		fallthrough
	default:
		defaultLogger.level = logx.DebugLevel
	}
}

func Debug(format string, args ...any) {
	if defaultLogger.level > logx.DebugLevel {
		return
	}
	logx.Debug(fmt.Sprintf(prefix+format, args...))
}
func Info(format string, args ...any) {
	if defaultLogger.level > logx.InfoLevel {
		return
	}
	logx.Info(fmt.Sprintf(prefix+format, args...))
}
func Warn(format string, args ...any) {
	if defaultLogger.level > logx.InfoLevel {
		return
	}
	logx.Alert(fmt.Sprintf(prefix+format, args...))
}
func Error(format string, args ...any) {
	if defaultLogger.level > logx.ErrorLevel {
		return
	}
	logx.Error(fmt.Sprintf(prefix+format, args...))
}
