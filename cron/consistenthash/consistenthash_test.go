package consistenthash

import (
	"testing"
)

func TestNew(t *testing.T) {
	nodes := New(3, nil)
	nodes.Add("192.168.137.1", "192.168.137.2", "192.168.137.3", "192.168.137.4", "192.168.137.5")
	nodes.Del("192.168.137.3")
	t.Log("val: ", nodes.Get("zackeus"))
}
