package re

type Pattern string

const (

	// PatternUUID UUID 提取正则
	PatternUUID Pattern = "[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}"
)
