package events

const (
	// 呼叫中心事件明细

	// ActionAgentStatusChange 座席状态变更
	ActionAgentStatusChange = "agent-status-change"
	// ActionAgentStateChange 座席呼叫状变更
	ActionAgentStateChange = "agent-state-change"
	// ActionAgentOffering 座席分配话务(来电)
	ActionAgentOffering = "agent-offering"
	// ActionBridgeAgentStart 座席桥接
	ActionBridgeAgentStart = "bridge-agent-start"
	// ActionBridgeAgentEnd 座席通话结束
	ActionBridgeAgentEnd = "bridge-agent-end"
	// ActionBridgeAgentFail 座席桥接失败
	ActionBridgeAgentFail = "bridge-agent-fail"
	// ActionMembersCount 排队成员数
	ActionMembersCount = "members-count"
	// ActionMemberQueueStart 入队
	ActionMemberQueueStart = "member-queue-start"
	// ActionMemberQueueEnd 出队
	ActionMemberQueueEnd = "member-queue-end"
	// ActionMemberQueueResume 复位
	ActionMemberQueueResume = "member-queue-resume"
)
