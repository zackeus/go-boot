package drive

import "context"

type (
	EventType string

	Event struct {
		Type EventType
		Node string
	}
)

var (
	AddNodeEvent EventType = "add"
	DelNodeEvent EventType = "del"
)

// Driver There is only one driver for one dcron.
// Tips for write a user-defined Driver by yourself.
//  1. Confirm that `Stop` and `Start` can be called for more times.
//  2. Must make `GetNodes` will return error when timeout.
type Driver interface {
	Available() bool
	// NodeID get nodeID
	NodeID() string
	// GetNodes get nodes
	GetNodes() (nodes []string, err error)

	// Start register node to remote server (like etcd/redis),
	// will create a goroutine to keep the connection.
	// And then continue for other work.
	Start(ctx context.Context, ch chan<- *Event) (err error)

	// Stop stop the goroutine of keep connection.
	Stop() (err error)
}
