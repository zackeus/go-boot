package copier

import (
	"database/sql"
	"errors"
	"github.com/jinzhu/copier"
	"time"
)

type Option copier.Option

var (
	defaultOption = Option{
		IgnoreEmpty: false,
		/* 深拷贝 不会修改原有对象 */
		DeepCopy: true,
		Converters: []copier.TypeConverter{
			{
				SrcType: time.Time{},
				DstType: uint64(0),
				Fn: func(src any) (any, error) {
					/* 时间格式自定义转换 */
					s, ok := src.(time.Time)
					if !ok {
						return nil, errors.New("src type not matching")
					}
					return uint64(s.UnixMilli()), nil
				},
			},
			{
				SrcType: "",
				DstType: sql.NullString{},
				Fn: func(src any) (any, error) {
					s, ok := src.(string)
					if !ok {
						return nil, errors.New("src type not matching")
					}
					return sql.NullString{String: s, Valid: true}, nil
				},
			},
		},
	}
)

// Copy 对象拷贝
func Copy(dst any, src any, option ...Option) error {
	if option == nil {
		return copier.CopyWithOption(dst, src, copier.Option(defaultOption))
	}
	return copier.CopyWithOption(dst, src, copier.Option(option[0]))
}
