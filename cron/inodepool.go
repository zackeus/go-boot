package cron

import "time"

type INodePool interface {
	Start() error
	CheckJobAvailable(jobName string) (bool, error)
	Stop()

	GetNodeID() string
	GetLastNodesUpdateTime() time.Time
}
