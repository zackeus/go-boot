package command

import "testing"

func TestAPI_BuildMessage(t *testing.T) {
	api := API{
		Command:   "originate",
		Arguments: "user/1000 &echo",
	}
	t.Log(api.BuildMessage())
	t.Log(api.BuildCmd())
}
