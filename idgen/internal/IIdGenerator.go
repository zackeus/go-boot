/*
 * 版权属于：yitter(yitter@126.com)
 * 代码编辑：guoyahao
 * 代码修订：yitter
 * 开源地址：https://github.com/yitter/idgenerator
 */

package internal

import "time"

type IIdGenerator interface {
	NextId() int64
	ExtractTime(id int64) time.Time
}
