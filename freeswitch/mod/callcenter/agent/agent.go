package agent

type (
	// Type 坐席类型
	Type string
	// Status 坐席状态
	Status string
	// State 坐席呼叫状态
	State string
)

const (
	// TypeCallBack 根据 contact 字段呼叫客户端
	TypeCallBack Type = "callback"
	// TypeUuidStandby 使用 agent uuid 直接桥接电话
	TypeUuidStandby Type = "uuid-standby"
)

const (
	// StatusIdle 示闲 可以接收队列呼叫
	StatusIdle Status = "Available"
	// StatusAvailable 可用 一旦呼叫结束，State 将被设置为'Idle'(不会自动设置为'Waiting')
	StatusAvailable Status = "Available (On Demand)"
	// StatusBusy 示忙 仍然在线 但不会接收队列呼叫
	StatusBusy Status = "On Break"
	// StatusLogout 签出 无法接收队列呼叫
	StatusLogout Status = "Logged Out"
)

const (
	// StateIdle 闲置 不会分配话务
	StateIdle State = "Idle"
	// StateWaiting 等待接受呼叫
	StateWaiting State = "Waiting"
	// StateReceiving 正在接受呼叫
	StateReceiving State = "Receiving"
	// StateCalling 通话中
	StateCalling State = "In a queue call"
)
