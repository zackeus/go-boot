package aes

import (
	"crypto/aes"
	"crypto/cipher"
	"errors"
	"gitee.com/zackeus/goutil/encodes/secutil"
	"sync"
)

type (
	// Method 加密模式
	Method string

	Option func(c *Cipher) error

	Cipher struct {
		once      sync.Once
		key       []byte // 密钥
		iv        []byte // 初始化向量(长度必须与key相同)
		core      cipher.Block
		method    Method
		blockMode struct {
			encrypter cipher.BlockMode
			decrypter cipher.BlockMode
		}
		stream struct {
			encrypter cipher.Stream
			decrypter cipher.Stream
		}
	}
)

const (
	// MethodECB 电码本模式 密文被分割成分组长度相等的块（不足补齐）然后单独一个个加密，一个个输出组成密文
	MethodECB Method = "ECB"
	// MethodCBC 密码分组链接模式 前一个分组的密文和当前分组的明文异或或操作后再加密
	MethodCBC Method = "CBC"
	// MethodCFB 密码反馈模式
	MethodCFB Method = "CFB"
)

func New(key []byte, opts ...Option) (*Cipher, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	c := &Cipher{
		key:    key,
		iv:     key[:block.BlockSize()],
		core:   block,
		method: MethodCBC,
	}

	for _, opt := range opts {
		if err := opt(c); err != nil {
			return nil, err
		}
	}

	if err := c.init(); err != nil {
		return nil, err
	}
	return c, nil
}

// 初始化
func (c *Cipher) init() error {
	var err error
	c.once.Do(func() {
		switch c.method {
		case MethodCBC:
			c.blockMode = struct {
				encrypter cipher.BlockMode
				decrypter cipher.BlockMode
			}{encrypter: cipher.NewCBCEncrypter(c.core, c.iv), decrypter: cipher.NewCBCDecrypter(c.core, c.iv)}
		case MethodCFB:
			c.stream = struct {
				encrypter cipher.Stream
				decrypter cipher.Stream
			}{encrypter: cipher.NewCFBEncrypter(c.core, c.iv), decrypter: cipher.NewCFBDecrypter(c.core, c.iv)}
		case MethodECB:
		default:
			err = errors.New("unsupported method")
		}
	})

	return err
}

// Encrypt 加密
func (c *Cipher) Encrypt(src []byte) ([]byte, error) {
	var encrypted []byte
	blockSize := c.core.BlockSize()

	switch c.method {
	case MethodCBC:
		/* CBC */
		/* 去除补全码 */
		padSrc := secutil.PKCS5Padding(src, blockSize)
		encrypted = make([]byte, len(padSrc))
		c.blockMode.encrypter.CryptBlocks(encrypted, padSrc)
	case MethodCFB:
		/* CFB */
		encrypted = make([]byte, len(src))
		c.stream.encrypter.XORKeyStream(encrypted, src)
	case MethodECB:
		/* ECB */
		/* 去除补全码 */
		padSrc := secutil.PKCS5Padding(src, blockSize)
		encrypted = make([]byte, len(padSrc))
		/* 分组分块加密 */
		for bs, be := 0, blockSize; bs < len(padSrc); bs, be = bs+blockSize, be+blockSize {
			c.core.Encrypt(encrypted[bs:be], padSrc[bs:be])
		}
	default:
		return nil, errors.New("unsupported method")
	}

	return encrypted, nil
}

// Decrypt 解密
func (c *Cipher) Decrypt(enc []byte) ([]byte, error) {
	srcBytes := make([]byte, len(enc))

	switch c.method {
	case MethodCBC:
		/* CBC */
		c.blockMode.decrypter.CryptBlocks(srcBytes, enc)
		return secutil.PKCS5UnPadding(srcBytes)
	case MethodCFB:
		/* CFB */
		c.stream.decrypter.XORKeyStream(srcBytes, enc)
		return srcBytes, nil
	case MethodECB:
		/* ECB */
		// 分组分块解密
		for bs, be := 0, c.core.BlockSize(); bs < len(enc); bs, be = bs+c.core.BlockSize(), be+c.core.BlockSize() {
			c.core.Decrypt(srcBytes[bs:be], enc[bs:be])
		}
		return secutil.PKCS5UnPadding(srcBytes)
	default:
		return nil, errors.New("unsupported method")
	}
}

// WithIV 设置初始化向量
func WithIV(iv []byte) Option {
	return func(c *Cipher) error {
		if len(iv) != c.core.BlockSize() {
			return errors.New("IV length must equal block size")
		}
		c.iv = iv
		return nil
	}
}

// WithMethod 设置模式
func WithMethod(m Method) Option {
	return func(c *Cipher) error {
		c.method = m
		return nil
	}
}
