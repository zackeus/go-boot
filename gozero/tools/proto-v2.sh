#!/usr/bin/env bash
# protobuf 生成脚本(根据项目结构调整 DIR)
# sh proto.sh

# 目录格式 ${PROJECT}/${NAME}/rpc
PROJECT="csp"
NAME="drs"
MOD="$GOPATH"/pkg/mod

# 工作路径 /Users/zackeus/golang/workspaces
WORK_DIR=$(cd "$(dirname "$0")" || return;cd ../../../../; pwd)
# rpc 路径
RPC_DIR="$WORK_DIR/$PROJECT/$NAME/rpc"

#数据输出
#echo "$RPC_DIR"/protobuf/types/"$1".proto
#echo $RPC_DIR

# 生成 proto types
for PROTO_FILE in "$RPC_DIR"/protobuf/types/*.proto
do
  # proto 生成
  protoc --go_out="$RPC_DIR" --go-grpc_out="$RPC_DIR" -I "$WORK_DIR" -I "$MOD" "$PROTO_FILE"
done
# 自定义 tag
protoc-go-inject-tag -input="$RPC_DIR"/proto/*.pb.go
goctl rpc protoc "$RPC_DIR"/protobuf/$NAME.proto --go_out="$RPC_DIR" --go-grpc_out="$RPC_DIR" --zrpc_out="$RPC_DIR" -I "$WORK_DIR" -I "$MOD" -style gozero -m --home="/Users/zackeus/.goctl"
