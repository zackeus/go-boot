package gob

import (
	"bytes"
	"encoding/gob"
	"time"
)

// GobCodec is used for encoding/decoding session data to and from a byte
// slice using the encoding/gob package.
type GobCodec struct{}

// Encode converts a session deadline and values into a byte slice.
func (GobCodec) Encode(uid string, deadline time.Time, values map[string]any) ([]byte, error) {
	aux := &struct {
		Uid      string
		Deadline time.Time
		Values   map[string]any
	}{
		Uid:      uid,
		Deadline: deadline,
		Values:   values,
	}

	var b bytes.Buffer
	if err := gob.NewEncoder(&b).Encode(&aux); err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}

// Decode converts a byte slice into a session deadline and values.
func (GobCodec) Decode(b []byte) (string, time.Time, map[string]interface{}, error) {
	aux := &struct {
		Uid      string
		Deadline time.Time
		Values   map[string]interface{}
	}{}

	r := bytes.NewReader(b)
	if err := gob.NewDecoder(r).Decode(&aux); err != nil {
		return "", time.Time{}, nil, err
	}

	return aux.Uid, aux.Deadline, aux.Values, nil
}
