package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-zero/core/logx"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.71:8021")
	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	defer inbound.Close()

	ctx := context.Background()

	uuid := "aa34db18-3fcc-4657-8474-4ed5fb009d7a"
	/* 呼叫保持 */
	if err := inbound.Hold(ctx, uuid); err != nil {
		logx.Error(err)
	}

}
