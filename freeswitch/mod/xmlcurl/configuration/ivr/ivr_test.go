package ivr

import (
	"os"
	"testing"
)

func TestNew(t *testing.T) {
	x, err := New()
	if err != nil {
		t.Error(err)
		return
	}

	menu := CreateMenu("test", 3)
	menu.AddChild(CreateEntry("4", ActionExecApp))
	x.BodyElement().AddChild(menu)

	if p, ok := x.(*Xml); ok {
		p.doc.Indent(1)
		_, _ = p.doc.WriteTo(os.Stdout)
	}
}
