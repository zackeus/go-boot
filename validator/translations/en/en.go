package en

import (
	"gitee.com/zackeus/go-boot/validator/tags"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	translations "github.com/go-playground/validator/v10/translations/en"
)

func RegisterDefaultTranslations(v *validator.Validate, trans ut.Translator) error {
	if err := translations.RegisterDefaultTranslations(v, trans); err != nil {
		return err
	}

	if err := v.RegisterTranslation(tags.TagFieldLen, trans, func(ut ut.Translator) error {
		return ut.Add(tags.TagFieldLen, "{0} must be {1} in length", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(tags.TagFieldLen, fe.Field(), fe.Param())
		return t
	}); err != nil {
		return err
	}

	if err := v.RegisterTranslation(tags.TagChoiceSingleDigit, trans, func(ut ut.Translator) error {
		return ut.Add(tags.TagChoiceSingleDigit, "{0} must be within the specified range", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(tags.TagChoiceSingleDigit, fe.Field())
		return t
	}); err != nil {
		return err
	}

	if err := v.RegisterTranslation(tags.TagDtmf, trans, func(ut ut.Translator) error {
		return ut.Add(tags.TagDtmf, "{0} invalid dtmf type", true)
	}, func(ut ut.Translator, fe validator.FieldError) string {
		t, _ := ut.T(tags.TagDtmf, fe.Field())
		return t
	}); err != nil {
		return err
	}
	return nil
}
