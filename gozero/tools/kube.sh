#!/usr/bin/env bash
# go-zero kube 生成脚本

# $1: 项目名
# $2: 镜像名
# $3: 版本号
# $4: 端口
goctl kube deploy -namespace "$1" -name "$2" -image "registry.zackeus.com/$1/$2:$3" -port "$4" -o ./kube/"$2".yaml --home="/Users/zackeus/.goctl"