package scs

import (
	"fmt"
	"gitee.com/zackeus/go-boot/scs/store/redisstore"
	"gitee.com/zackeus/go-zero/core/stores/redis"
	"log"
	"net/http"
	"testing"
	"time"
)

var sessionManager *SessionManager

// 登录
func loginHandler(w http.ResponseWriter, r *http.Request) {
	/* session 生成 */
	ctx := sessionManager.Generate(r.Context(), "17037")
	r = r.WithContext(ctx)
	/* 存值 */
	_ = sessionManager.Put(r.Context(), "userName", "zackeus")

	/* 存储 session */
	token, expiry, err := sessionManager.Commit(r.Context())
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	/* 设置 response */
	_ = sessionManager.WriteSessionCookie(r.Context(), w, token, expiry)

}

func testHandler(w http.ResponseWriter, r *http.Request) {
	uid, err := sessionManager.Uid(r.Context())
	if err != nil {
		fmt.Println("取值失败：", err)
	}

	err = sessionManager.Iterate(r.Context(), func(uid string, token string, deadline time.Time, values map[string]any) error {
		userName, _ := values["userName"]
		fmt.Println(" 在线用户： ", uid, " : ", userName)
		return nil
	}, "17037")
	if err != nil {
		fmt.Println("遍历失败：", err)
	}

	_, _ = w.Write([]byte("my no is : " + uid))
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	uid, err := sessionManager.Uid(r.Context())
	if err != nil {
		fmt.Println("取值失败：", err)
	}
	err = sessionManager.Destroy(r.Context())
	if err != nil {
		fmt.Println("销毁失败：", err)
	}
	_, _ = w.Write([]byte("logout no is : " + uid))
}

/* scs 异常处理 */
func sessionErrorFunc(w http.ResponseWriter, r *http.Request, err error) {
	fmt.Println("server error: ", err)
	log.Output(2, err.Error())
	http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
}

func TestSession(t *testing.T) {
	client, err := redis.NewRedis(redis.RedisConf{
		Host: "redis.server:6379",
		Pass: "yulon123",
		Type: "node",
	})
	if err != nil {
		fmt.Println("err: ", err)
		return
	}
	store, err := redisstore.New("scs:session:", client)
	if err != nil {
		fmt.Println("err: ", err)
		return
	}

	// Initialize a new scs manager and configure the scs lifetime.
	sessionManager, err = New(
		"session_id",
		60*time.Minute,
		store,
		WithIdleTimeout(30*time.Minute),
		WithSameSite(http.SameSiteStrictMode),
		WithErrorCallback(sessionErrorFunc),
	)
	if err != nil {
		fmt.Println(err)
		return
	}

	mux := http.NewServeMux()
	mux.HandleFunc("/test", testHandler)
	mux.HandleFunc("/logout", logoutHandler)
	http.Handle("/", sessionManager.Handle(mux))

	http.HandleFunc("/login", loginHandler)
	http.ListenAndServe(":4000", nil)
}
