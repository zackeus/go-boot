package filex

import (
	"fmt"
	"testing"
)

func TestReadStream(t *testing.T) {
	src := "/Users/zackeus/mnt/FS/voice/zh/cn/zackeus/conference/8000/conf-enter_conf_number.wav"
	dst := "/Users/zackeus/Downloads/test.wav"

	writer, err := NewWriter(dst)
	if err != nil {
		fmt.Println("WriteDataToTxt os.OpenFile() err:", err)
		return
	}
	defer func(writer *Writer) {
		_ = writer.Close()
	}(writer)

	t.Run("reader_test", func(t *testing.T) {
		if err := ReadPath(src, func(content []byte) error {
			_, err := writer.Write(content)
			if err != nil {
				t.Errorf("writer write err: %v\n", err)
				return err
			}
			return nil
		}); err != nil {
			t.Errorf("ReadStream err: %v\n", err)
		}
	})
}
