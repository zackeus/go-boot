package vars

const (
	// ACTION_CREATE 创建 如果需要创建的文档已经存在，那么创建失败
	ACTION_CREATE = "create"
	// ACTION_DELETE 删除
	ACTION_DELETE = "delete"
	// ACTION_INDEX 创建或替换 如果要创建的文档不存在则执行创建操作，如果已经存在则执行替换操作
	ACTION_INDEX = "index"
	// ACTION_UPDATE 更新
	ACTION_UPDATE = "update"
)
