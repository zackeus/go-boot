package validator

import (
	"github.com/go-playground/locales/en"
	"github.com/go-playground/locales/zh"
	"github.com/stretchr/testify/assert"
	"testing"
)

type UserModel struct {
	Name     string `label:"姓名" json:"name" validate:"required,len=3"`
	Age      int64  `label:"年龄" json:"age" validate:"required,fieldlen=2"`
	Birthday string `label:"生日" json:"birthday" validate:"required"`
	Choice   string `label:"选择范围" json:"choice" validate:"choiceSingle"`
	Key      string `label:"DTMF 按键" json:"key" validate:"dtmf"`
}

func TestValidate_Struct(t *testing.T) {
	//获取验证对象
	validate, err := New(en.New(), zh.New())
	assert.Empty(t, err)

	//创建一个验证数据
	user := UserModel{
		Name:     "aaa",
		Age:      18,
		Birthday: "11",
		Choice:   "1-9",
		Key:      "c",
	}

	err = validate.Struct(user)
	assert.Empty(t, err)
}
