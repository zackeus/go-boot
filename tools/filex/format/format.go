package format

// FileFormat 文件格式
type FileFormat string

const (
	MP3 FileFormat = "mp3"
	WAV FileFormat = "wav"
	PCM FileFormat = "pcm"
)
