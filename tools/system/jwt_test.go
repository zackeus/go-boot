package system

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var uid = "17037"
var secret = "123456789*"
var userName = "17037"
var name = "张舟"
var roles = []string{"role1", "role2"}
var permissions = []string{"per1", "per2"}

func TestGenerateToken(t *testing.T) {
	token, err := GenerateToken(uid, secret, 3*time.Second, WithJwtUserName(userName), WithJwtName(name), WithJwtRoles(roles), WithJwtPermissions(permissions))
	fmt.Println(err)
	assert.Empty(t, err)
	t.Log(token)
}
