package aes

import (
	"encoding/base64"
	"encoding/hex"
	"github.com/stretchr/testify/assert"
	"testing"
)

var (
	src = []byte("Hello World")      // 待加密的数据
	key = []byte("ABCDEFGHIJKLMNOP") // 加密的密钥
)

func TestCBC(t *testing.T) {
	c, err := New(key, WithMethod(MethodCBC), WithIV([]byte("ABCDEFGHIJ2LMNO1")))
	assert.Empty(t, err)

	/* 加密 */
	es, err := c.Encrypt(src)
	assert.Empty(t, err)
	t.Log("密文(hex)：", hex.EncodeToString(es))
	t.Log("密文(base64)：", base64.StdEncoding.EncodeToString(es))

	/* 解密 */
	ds, err := c.Decrypt(es)
	assert.Empty(t, err)
	t.Log("解密结果：", string(ds))
}

func TestECB(t *testing.T) {
	c, err := New(key, WithMethod(MethodECB))
	assert.Empty(t, err)

	/* 加密 */
	es, err := c.Encrypt(src)
	assert.Empty(t, err)
	t.Log("密文(hex)：", hex.EncodeToString(es))
	t.Log("密文(base64)：", base64.StdEncoding.EncodeToString(es))

	/* 解密 */
	ds, err := c.Decrypt(es)
	assert.Empty(t, err)
	t.Log("解密结果：", string(ds))
}

func TestCFB(t *testing.T) {
	c, err := New(key, WithMethod(MethodCFB))
	assert.Empty(t, err)

	/* 加密 */
	es, err := c.Encrypt(src)
	assert.Empty(t, err)
	t.Log("密文(hex)：", hex.EncodeToString(es))
	t.Log("密文(base64)：", base64.StdEncoding.EncodeToString(es))

	/* 解密 */
	ds, err := c.Decrypt(es)
	assert.Empty(t, err)
	t.Log("解密结果：", string(ds))
}
