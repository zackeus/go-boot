// Copyright 2012-present Oliver Eilhard. All rights reserved.
// Use of this source code is governed by a MIT-license.
// See http://olivere.mit-license.org/license.txt for details.

package helper

import "gitee.com/zackeus/go-boot/tools/jsonx"

// PinnedQuery is a query that promotes selected documents to rank higher than those matching a given query.
//
// For more details, see:
// https://www.elastic.co/guide/en/elasticsearch/reference/7.8/query-dsl-pinned-query.html
type PinnedQuery struct {
	ids     []string
	organic Query
}

// NewPinnedQuery creates and initializes a new pinned query.
func NewPinnedQuery() *PinnedQuery {
	return &PinnedQuery{}
}

// Ids sets an array of document IDs listed in the order they are to appear in results.
func (q *PinnedQuery) Ids(ids ...string) *PinnedQuery {
	q.ids = ids
	return q
}

// Organic sets a choice of query used to rank documents which will be ranked below the "pinned" document ids.
func (q *PinnedQuery) Organic(m Query) *PinnedQuery {
	q.organic = m
	return q
}

// Source returns the JSON serializable content for this query.
func (q *PinnedQuery) Source() (interface{}, error) {
	// {
	// 	  "pinned": {
	// 	  	"ids": [ "1", "4", "100" ],
	// 	  	"organic": {
	// 		  "match": {
	// 		    "description": "iphone"
	// 		  }
	// 		}
	//    }
	// }

	qs := make(map[string]interface{})
	params := make(map[string]interface{})
	qs["pinned"] = params
	if len(q.ids) > 0 {
		params["ids"] = q.ids
	}
	if q.organic != nil {
		src, err := q.organic.Source()
		if err != nil {
			return nil, err
		}
		params["organic"] = src
	}

	return qs, nil
}

func (q *PinnedQuery) Build() (string, error) {
	source, err := q.Source()
	if err != nil {
		return "", err
	}

	qs := make(map[string]interface{})
	qs["query"] = source

	s, err := jsonx.MarshalToString(qs)
	if err != nil {
		return "", err
	}
	return s, nil
}
