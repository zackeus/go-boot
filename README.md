# csp
go test -v -cover -covermode=atomic -coverprofile=coverage.out -coverpkg=... redis_test.go
go tool cover -html=coverage.out -o build.html

# 安装 goctl
go install github.com/zeromicro/go-zero/tools/goctl@latest
# 模板初始化
goctl template init
# 复制当前版本模板
cp -R /Users/zackeus/.goctl/${version} /Users/zackeus/golang/workspaces/go-boot/gozero/template/release
# 模板替换
rm -rf /Users/zackeus/.goctl/${version}
cp -R /Users/zackeus/golang/workspaces/go-boot/gozero/template/release /Users/zackeus/.goctl/${version}
# 版本迁移
goctl migrate --version v1.5.5

# 依赖测试
replace gitee.com/zackeus/goutil v0.6.18-release => /Users/zackeus/golang/workspaces/goutil
replace gitee.com/zackeus/go-boot v1.5.3 => /Users/zackeus/golang/workspaces/go-boot
replace gitee.com/zackeus/go-zero v1.8.0-release => /Users/zackeus/golang/workspaces/go-zero