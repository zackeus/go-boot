package codes

import "net/http"

const (
	// OK is returned on success.
	OK int = 0

	// Unauthorized 未鉴权
	Unauthorized int = http.StatusUnauthorized
	// Forbidden 无权限
	Forbidden int = http.StatusForbidden
	// UnprocessableEntity 参数校验异常
	UnprocessableEntity int = http.StatusUnprocessableEntity
	// ServerErr 服务器异常
	ServerErr int = http.StatusInternalServerError

	// NoExist 数据不存在
	NoExist int = 10001
	// NoAvailable 数据不可用
	NoAvailable int = 10002
	// RepeatData 数据已存在
	RepeatData = 10003
)
