package freeswitch

type (
	// Application 执行应用
	Application string
	// HangupCause 挂断原因
	HangupCause string
	// RecordCompletionCause 录音完成原因
	RecordCompletionCause string

	CallType string
	// CallState 分机呼叫状态
	CallState string
	// ActionCode 执行代号
	ActionCode string
)

const (
	// AppAnswer 应答一个信道的呼叫
	AppAnswer Application = "answer"
	// AppAttXfer 呼叫转移
	AppAttXfer Application = "att_xfer"

	// AppBindDigitAction 绑定一个关键字队列或者一个正则表达式到一个action上
	AppBindDigitAction Application = "bind_digit_action "
	// AppBindMetaApp 在桥接或者执行另一个dialplan APP期间，对特定的呼叫leg上应答输入的DTMF按键序列
	AppBindMetaApp Application = "bind_meta_app"
	// AppBlockDtmf 阻止DTMF在频道上发送或接收
	AppBlockDtmf Application = "block_dtmf"
	// AppBreak 终断信道中正在运行的应用
	AppBreak Application = "break"
	// AppBridge 将信道桥接到另一个已存在的信道上（即产生通话）
	AppBridge Application = "bridge"
	// AppBridgeExport 通过桥接（bridge）导出信道变量
	AppBridgeExport Application = "bridge_export"

	// AppCapture 将捕获到的数字添加到信道变量数组中
	AppCapture Application = "capture"
	// AppChat 通过IM客户端发送一个文本消息
	AppChat Application = "chat"
	// AppCheckAcl –根据访问控制列表中的呼叫发起方地址是否符合规则，然后执行允许或者拒绝的操作
	AppCheckAcl Application = "check_acl"
	// AppClearDigitAction 清空所有的数字绑定
	AppClearDigitAction Application = "clear_digit_action"
	// AppClearSpeechCache 清除语音句柄缓存
	AppClearSpeechCache Application = "clear_speech_cache"
	AppCluechoo         Application = "cluechoo"
	// AppCngPlc 舒适噪音生成的配置
	AppCngPlc Application = "cng_plc"
	// AppConference 会议呼叫
	AppConference Application = "conference"

	// AppDB 插入数据到数据库中
	AppDB Application = "db"
	// AppDeflect 挂断当前通话并向通话发起者（是一个网关或者代）发送一个REFER消息和一个新的INVITE消息
	AppDeflect Application = "deflect"
	// AppDelayEcho 延迟一定事件后ECHO一个音频文件
	AppDelayEcho Application = "delay_echo"
	// AppDetectSpeech 实现语音识别
	AppDetectSpeech Application = "detect_speech"
	// AppDigitActionSetRealm 更改绑定域
	AppDigitActionSetRealm Application = "digit_action_set_realm"
	// AppDisplaceSession 替换（部分替换）信道中的文件或流
	AppDisplaceSession Application = "displace_session"

	// AppEarlyHangup 激活信道中的早期挂断（缺少相应资料）
	AppEarlyHangup Application = "early_hangup"
	// AppEavesdrop 监控一个信道
	AppEavesdrop Application = "eavesdrop"
	// AppEcho 向发起方回应音频或者视频
	AppEcho Application = "echo"
	// AppEnableHeartbeat 激活媒体的心跳机制（维持与媒体的连接）
	AppEnableHeartbeat Application = "enable_heartbeat"
	// AppEndlessPlayback 向呼叫方无限播放一个文件
	AppEndlessPlayback Application = "endless_playback"
	// AppEnum 执行 E.164查找
	AppEnum Application = "enum"
	// AppErlang 用Erlang处理一个呼叫
	AppErlang Application = "erlang"
	// AppEval 执行一个内部的API
	AppEval Application = "eval"
	// AppEvent 产生（file）一个事件
	AppEvent Application = "event"
	// AppExecuteExtension 在一个表达式中执行另一个表达式并得到其返回值，就像执行宏一样执行完后返回
	AppExecuteExtension Application = "execute_extension"
	// AppExport 通过桥接导出信号变量
	AppExport Application = "export"

	// AppFaxDetect 检查传真的CNG – 可能已被废弃
	AppFaxDetect Application = "fax_detect"
	// AppFifo 向FIFO队列发送一个呼叫
	AppFifo Application = "fifo"
	// AppFifoTrackCall 像FIFO呼叫队列一样手工计数队列
	AppFifoTrackCall Application = "fifo_track_call"
	// AppFlushDtmf 输出所有在队列中等待的DTMF按键输入
	AppFlushDtmf Application = "flush_dtmf"

	// AppGentones 生成一个TGML音调
	AppGentones Application = "gentones"
	// AppGroup 在组插入或者删除成员
	AppGroup Application = "group"

	// AppHangup 挂断当前信道
	AppHangup Application = "hangup"
	// AppHash 向数据库（db）中添加一个哈希值（hash）
	AppHash Application = "hash"
	// AppHold 发送一个等待消息
	AppHold Application = "hold"
	// AppHttapi 用HTTAPI向Web服务器发送一个呼叫控制
	AppHttapi Application = "httapi"

	// AppInfo 显示呼叫信息
	AppInfo Application = "info"
	// AppIntercept 如果知道一个信道的uuid，则可用于掌管这个通话并接听这个通话
	AppIntercept Application = "intercept"
	// AppIvr 运行一个IVR菜单
	AppIvr Application = "ivr"

	// AppJavascript 在diaplan中运行一个JavaScript脚本
	AppJavascript Application = "javascript"
	// AppJitterbuffer 向一个会话（Session）发送一个抖动缓冲（jitter buffer）消息
	AppJitterbuffer Application = "jitterbuffer"

	// AppLimit 限制某段号码对某个资源的访问权限
	AppLimit Application = "limit"
	// AppLimitExecute 有限制的执行某个应用
	AppLimitExecute Application = "limit_execute"
	// AppLimitHash 设置对资源调用/从资源调用的次数限制
	AppLimitHash Application = "limit_hash"
	// AppLimitHashExecute 对某个应用设置限制
	AppLimitHashExecute Application = "limit_hash_execute"
	// AppLog 向终端打印一个日志文本
	AppLog Application = "log"
	// AppLua 在dialplan中执行LUA脚本
	AppLua Application = "lua"

	// AppMediaReset 重设所有的分支或者代理的媒体标记位
	AppMediaReset Application = "media_reset"
	// AppMkdir 创建一个目录
	AppMkdir Application = "mkdir"
	// AppMultiset 在一个动作（Action）中设置多个信道变量
	AppMultiset Application = "multiset"
	// AppMutex 互斥锁，仅允许在同一个事件内允许一个呼叫，阻塞其他所有的呼叫
	AppMutex Application = "mutex"

	// AppPage 在被应答的等待时间中，向信道列表播放录音文件
	AppPage Application = "page"
	// AppPark 悬空一个呼叫
	AppPark Application = "park"
	// AppParkState 悬空状态
	AppParkState Application = "park_state"
	// AppPhrase 说出一个句子（phrase）
	AppPhrase Application = "phrase"
	// AppPickup 允许接听方为一个或者多个分组，反之即可以同时向一个分组发起呼叫
	AppPickup Application = "pickup"
	// AppPlayAndDetectSpeech 在语音识别处理时播放文件
	AppPlayAndDetectSpeech Application = "play_and_detect_speech"
	// AppPlayAndGetDigits 播放一个音频并捕获数字
	AppPlayAndGetDigits Application = "play_and_get_digits"
	// AppPlayFsv 播放一个FSV文件. FSV - (FS视频文件格式)
	AppPlayFsv Application = "play_fsv"
	// AppPlayback 向发起者播放一个声音文件
	AppPlayback Application = "playback"
	// AppPreAnswer 在早起媒体状状态下的预应答
	AppPreAnswer Application = "pre_answer"
	// AppPreprocess 暂缺描述
	AppPreprocess Application = "preprocess"
	// AppPresence 发送一个Presence
	AppPresence Application = "presence"
	// AppPrivacy 在呼叫中设置呼叫方策略
	AppPrivacy Application = "privacy"

	// AppQueueDtmf 桥接成功后发送DTMF数字
	AppQueueDtmf Application = "queue_dtmf"

	// AppRead 读取DTMF按键数字
	AppRead Application = "read"
	// AppRecord 在信道输入中录音
	AppRecord Application = "record"
	// AppRecordFsv 记录一个FSV 文件. FSV - (FS 视频文件格式)
	AppRecordFsv Application = "record_fsv"
	// AppRecordSession 会话录音
	AppRecordSession Application = "record_session"
	// AppRecoveryRefresh 发送一个复原更新
	AppRecoveryRefresh Application = "recovery_refresh"
	// AppRedirect 重定向消息
	AppRedirect Application = "redirect"
	// AppRegex 执行一个正则表达式
	AppRegex Application = "regex"
	// AppRemoveBugs 移除media bugs.
	AppRemoveBugs Application = "remove_bugs"
	// AppRename 重命名文件
	AppRename Application = "rename"
	// AppRespond 向会话发送一个应答消息
	AppRespond Application = "respond"
	// AppRingReady 在信道中执行Ring_Ready
	AppRingReady Application = "ring_ready"
	// AppRxfax 将接收到的传真保存为tif文件
	AppRxfax Application = "rxfax"

	// AppSay 根据预先录制的声音播放时间、IP地址、数字等
	AppSay Application = "say"
	// AppSchedBroadcast 计划广播计划
	AppSchedBroadcast Application = "sched_broadcast"
	// AppSchedCancel 取消一个计划中的广播或者呼叫转移
	AppSchedCancel Application = "sched_cancel"
	// AppSchedHangup 激活计划中的挂断
	AppSchedHangup Application = "sched_hangup"
	// AppSchedHeartbeat 激活计划中的心跳机制
	AppSchedHeartbeat Application = "sched_heartbeat"
	// AppSchedTransfer 激活计划中的呼叫转移
	AppSchedTransfer Application = "sched_transfer"
	// AppSendDisplay 发送一个带sipfrag的INFO包
	AppSendDisplay Application = "send_display"
	// AppSendDtmf 发送DTMF, 2833, 或者SIP Info数字
	AppSendDtmf Application = "send_dtmf"
	// AppSendInfo 向终端发送信息
	AppSendInfo Application = "send_info"
	// AppSessionLoglevel 设置会话日志级别
	AppSessionLoglevel Application = "session_loglevel"
	// AppSet 设置信道变量
	AppSet Application = "set"
	// AppSetAudioLevel 调整信道中音频的读或者写级别
	AppSetAudioLevel Application = "set_audio_level"
	// AppSetGlobal 设置全局变量
	AppSetGlobal Application = "set_global"
	// AppSetName 命名信道
	AppSetName Application = "set_name"
	// AppSetProfileVar 设置一个呼叫方配置变量
	AppSetProfileVar Application = "set_profile_var"
	// AppSetUser 设置一个用户信息（初始化）
	AppSetUser Application = "set_user"
	// AppSetZombieExec 在当前信道中设置僵尸执行标记位
	AppSetZombieExec Application = "set_zombie_exec"
	// AppSleep 暂停一个信道
	AppSleep Application = "sleep"
	// AppSocket 建立一个外联（outbound）套接字连接
	AppSocket Application = "socket"
	// AppSoundTest 音频分析
	AppSoundTest Application = "sound_test"
	// AppSpeak 根据预设的声音引擎播放文本
	AppSpeak Application = "speak"
	// AppSoftHold 将已桥接的信道置于等待状态
	AppSoftHold Application = "soft_hold"
	// AppStartDtmf 开始带内(inband)检测
	AppStartDtmf Application = "start_dtmf"
	// AppStopDtmf 结束带内(inband)检测
	AppStopDtmf Application = "stop_dtmf"
	// AppStartDtmfGenerate 开始带内DTMF生成
	AppStartDtmfGenerate Application = "start_dtmf_generate"
	// AppStopDisplaceSession 停止信道中的音频替换
	AppStopDisplaceSession Application = "stop_displace_session"
	// AppStopDtmfGenerate 停止带内DTMF生成
	AppStopDtmfGenerate Application = "stop_dtmf_generate"
	// AppStopRecordSession 停止会话录音
	AppStopRecordSession Application = "stop_record_session"
	// AppStopToneDetect 停止监听tone
	AppStopToneDetect Application = "stop_tone_detect"
	// AppStrftime 返回带格式的日期时间
	AppStrftime Application = "strftime"
	// AppSystem 执行系统命令
	AppSystem Application = "system"

	// AppThreeWay 根据UUID发起三方呼叫
	AppThreeWay Application = "three_way"
	// AppToneDetect 检测音调的存在，并在发现时执行命令
	AppToneDetect Application = "tone_detect"
	// AppTransfer 呼叫转移
	AppTransfer Application = "transfer"
	// AppTranslate 号码翻译
	AppTranslate Application = "translate"

	// AppUnbindMetaApp 解除按键绑定
	AppUnbindMetaApp Application = "unbind_meta_app"
	// AppUnset 回收一个变量
	AppUnset Application = "unset"
	// AppUnhold 发送一个取消持有（un-hold）消息
	AppUnhold Application = "unhold"

	// AppVerboseEvents 使所有事件详细（使所有变量都出现在该通道的每个事件中）
	AppVerboseEvents Application = "verbose_events"

	// AppWaitForSilence 当在信道上执行无声等待时暂停进程
	AppWaitForSilence Application = "wait_for_silence"
	// AppWaitForAnswer 当等待呼叫被应答前，暂停进程
	AppWaitForAnswer Application = "wait_for_answer"
)

const (
	// CallOutbound 呼出
	CallOutbound CallType = "OUTBOUND"
	// CallInbound 呼入
	CallInbound CallType = "INBOUND"
	// CallInner 内部呼叫
	CallInner CallType = "INNER"
	// CallSpy 监听
	CallSpy CallType = "SPY"
	// CallThreeWay 三方
	CallThreeWay CallType = "THREEWAY"
	// CallXfer 协商转
	CallXfer CallType = "XFER"
)

const (
	// HangupUnspecified 未指定 没有其他适用的原因代码
	HangupUnspecified HangupCause = "UNSPECIFIED"
	// HangupChannelUnacceptable 频道不可接受
	HangupChannelUnacceptable HangupCause = "CHANNEL_UNACCEPTABLE"
	// HangupCallAwardedDelivered 呼叫已授予，正在通过已建立的渠道进行传递
	HangupCallAwardedDelivered HangupCause = "CALL_AWARDED_DELIVERED"
	// HangupNormalClearing 正常通话清除
	HangupNormalClearing HangupCause = "NORMAL_CLEARING"
	// HangupResponseToStatusEnquiry 对状态查询的回复
	HangupResponseToStatusEnquiry HangupCause = "RESPONSE_TO_STATUS_ENQUIRY"
	// HangupAccessInfoDiscarded 访问信息被丢弃
	HangupAccessInfoDiscarded HangupCause = "ACCESS_INFO_DISCARDED"
	HangupPreEmpted           HangupCause = "PRE_EMPTED"
	// HangupFacilityNotSubscribed 请求的设施未订阅
	HangupFacilityNotSubscribed HangupCause = "FACILITY_NOT_SUBSCRIBED"
	// HangupChanNotImplemented 通道类型未实现
	HangupChanNotImplemented HangupCause = "CHAN_NOT_IMPLEMENTED"
	// HangupInvalidCallReference 无效的呼叫参考值
	HangupInvalidCallReference HangupCause = "INVALID_CALL_REFERENCE"
	// HangupInvalidMsgUnspecified 无效消息，未指定
	HangupInvalidMsgUnspecified HangupCause = "INVALID_MSG_UNSPECIFIED"
	// HangupMandatoryIeMissing 必填信息元素丢失
	HangupMandatoryIeMissing HangupCause = "MANDATORY_IE_MISSING"
	// HangupMessageTypeNonexist 消息类型不存在或未实现
	HangupMessageTypeNonexist HangupCause = "MESSAGE_TYPE_NONEXIST"
	// HangupWrongMessage 消息与呼叫状态或消息类型不兼容或不存在
	HangupWrongMessage HangupCause = "WRONG_MESSAGE"
	// HangupIeNonexist 信息元素/参数不存在或未实现
	HangupIeNonexist HangupCause = "IE_NONEXIST"
	// HangupInvalidIeContents 无效的信息元素内容
	HangupInvalidIeContents HangupCause = "INVALID_IE_CONTENTS"
	// HangupWrongCallState 消息与呼叫状态不兼容
	HangupWrongCallState HangupCause = "WRONG_CALL_STATE"
	// HangupMandatoryIeLengthError 参数不存在或未实现-传递
	HangupMandatoryIeLengthError HangupCause = "MANDATORY_IE_LENGTH_ERROR"
	// HangupProtocolError 协议错误
	HangupProtocolError HangupCause = "PROTOCOL_ERROR"
	// HangupInterWorking 互通
	HangupInterWorking HangupCause = "INTERWORKING"
	// HangupCrash 崩溃
	HangupCrash HangupCause = "CRASH"
	// HangupSystemShutdown 系统关闭
	HangupSystemShutdown HangupCause = "SYSTEM_SHUTDOWN"
	HangupLoseRace       HangupCause = "LOSE_RACE"
	// HangupManagerRequest api命令使其挂断时
	HangupManagerRequest HangupCause = "MANAGER_REQUEST"
	// HangupBlindTransfer 盲转
	HangupBlindTransfer    HangupCause = "BLIND_TRANSFER"
	HangupAttendedTransfer HangupCause = "ATTENDED_TRANSFER"
	// HangupAllottedTimeout 服务器取消了呼叫，因为目标通道花费了很长时间才能应答
	HangupAllottedTimeout HangupCause = "ALLOTTED_TIMEOUT"
	HangupUserChallenge   HangupCause = "USER_CHALLENGE"
	HangupMediaTimeout    HangupCause = "MEDIA_TIMEOUT"
	// HangupPickedOff 此原因意味着呼叫是通过从另一个分机截取来接听的
	HangupPickedOff HangupCause = "PICKED_OFF"
	// HangupUserNotRegistered 分机未注册
	HangupUserNotRegistered HangupCause = "USER_NOT_REGISTERED"
	HangupProgressTimeout   HangupCause = "PROGRESS_TIMEOUT"
	// HangupGatewayDown 网关已关闭
	HangupGatewayDown HangupCause = "GATEWAY_DOWN"
	// HangupInvalidGateway 无效的网关
	HangupInvalidGateway HangupCause = "INVALID_GATEWAY"

	/* ------------------------------  sip 404 ---------------------- */

	// HangupUnallocatedNumber 未分配号码
	HangupUnallocatedNumber HangupCause = "UNALLOCATED_NUMBER"
	// HangupNoRouteTransitNet 没有通往指定公交网络的路线
	HangupNoRouteTransitNet HangupCause = "NO_ROUTE_TRANSIT_NET"
	// HangupNoRouteDestination 没有通往目的地的路线
	HangupNoRouteDestination HangupCause = "NO_ROUTE_DESTINATION"

	/* ------------------------------  sip 408 ---------------------- */

	// HangupNoUserResponse 没有用户响应
	HangupNoUserResponse HangupCause = "NO_USER_RESPONSE"

	/* ------------------------------  sip 410 ---------------------- */

	// HangupNumberChanged 号码已更改
	HangupNumberChanged HangupCause = "NUMBER_CHANGED"
	// HangupRedirectionToNewDestination 使用该原因值来调用重定向机制，以请求呼叫中涉及的先前交换以将呼叫路由到新号码
	HangupRedirectionToNewDestination HangupCause = "REDIRECTION_TO_NEW_DESTINATION"

	/* ------------------------------  sip 480 ---------------------- */

	// HangupNormalUnspecified 正常，未指定
	HangupNormalUnspecified HangupCause = "NORMAL_UNSPECIFIED"
	// HangupNoAnswer 无用户应答
	HangupNoAnswer HangupCause = "NO_ANSWER"
	// HangupSubscriberAbsent 订户缺席
	HangupSubscriberAbsent HangupCause = "SUBSCRIBER_ABSENT"

	/* ------------------------------  sip 483 ---------------------- */

	// HangupExchangeRoutingError 无法到达用户指示的目的地
	HangupExchangeRoutingError HangupCause = "EXCHANGE_ROUTING_ERROR"

	/* ------------------------------  sip 484 ---------------------- */

	// HangupInvalidNumberFormat 无效的数字格式（地址不完整)
	HangupInvalidNumberFormat HangupCause = "INVALID_NUMBER_FORMAT"

	/* ------------------------------  sip 486 ---------------------- */

	// HangupUserBusy 用户忙
	HangupUserBusy HangupCause = "USER_BUSY"

	/* ------------------------------  sip 487 ---------------------- */

	// HangupOriginatorCancel 取消呼叫
	HangupOriginatorCancel HangupCause = "ORIGINATOR_CANCEL"

	/* ------------------------------  sip 488 ---------------------- */

	// HangupBearercapabilityNotimpl 承载能力未实现
	HangupBearercapabilityNotimpl HangupCause = "BEARERCAPABILITY_NOTIMPL"
	// HangupIncompatibleDestination 不兼容的目的地
	HangupIncompatibleDestination HangupCause = "INCOMPATIBLE_DESTINATION"

	/* ------------------------------  sip 501 ---------------------- */

	// HangupFacilityRejected 网络无法提供用户请求的补充服务
	HangupFacilityRejected HangupCause = "FACILITY_REJECTED"
	// HangupFacilityNotImplemented 请求的设施未实现
	HangupFacilityNotImplemented HangupCause = "FACILITY_NOT_IMPLEMENTED"
	// HangupServiceNotImplemented 未实施服务或选项，未指定
	HangupServiceNotImplemented HangupCause = "SERVICE_NOT_IMPLEMENTED"

	/* ------------------------------  sip 502 ---------------------- */

	// HangupNetworkOutOfOrder 网络故障
	HangupNetworkOutOfOrder HangupCause = "NETWORK_OUT_OF_ORDER"
	// HangupDestinationOutOfOrder 目的地故障
	HangupDestinationOutOfOrder HangupCause = "DESTINATION_OUT_OF_ORDER"

	/* ------------------------------  sip 503 ---------------------- */

	// HangupNormalCircuitCongestion 无可用的电路/通道
	HangupNormalCircuitCongestion HangupCause = "NORMAL_CIRCUIT_CONGESTION"
	// HangupNormalTemporaryFailure 暂时故障
	HangupNormalTemporaryFailure HangupCause = "NORMAL_TEMPORARY_FAILURE"
	// HangupSwitchCongestion 交换设备拥塞
	HangupSwitchCongestion HangupCause = "SWITCH_CONGESTION"
	// HangupRequestedChanUnavail 请求的电路/通道不可用
	HangupRequestedChanUnavail HangupCause = "REQUESTED_CHAN_UNAVAIL"
	// HangupBearercapabilityNotavail 承载能力目前不可用
	HangupBearercapabilityNotavail HangupCause = "BEARERCAPABILITY_NOTAVAIL"

	/* ------------------------------  sip 504 ---------------------- */

	// HangupRecoveryOnTimerExpire 计时器到期后恢复
	HangupRecoveryOnTimerExpire HangupCause = "RECOVERY_ON_TIMER_EXPIRE"

	/* ------------------------------  sip 603 ---------------------- */

	// HangupCallRejected 通话拒接
	HangupCallRejected HangupCause = "CALL_REJECTED"
)

const (
	CallStateDown    CallState = "DOWN"
	CallStateDialing CallState = "DIALING"
	// CallStateRinging 180 振铃
	CallStateRinging CallState = "RINGING"
	// CallStateRingWait 等待对端回复180/183
	CallStateRingWait CallState = "RING_WAIT"
	// CallStateEarly 183 彩铃
	CallStateEarly CallState = "EARLY"
	// CallStateActive 摘机应答
	CallStateActive CallState = "ACTIVE"
	// CallStateHeld 保持
	CallStateHeld CallState = "HELD"
	// CallStateUnheld 取消保持
	CallStateUnheld CallState = "UNHELD"
	// CallStateIdle 挂机
	CallStateIdle CallState = "HANGUP"
)

const (
	// ActionHangUp 挂机
	ActionHangUp ActionCode = "1000"
	// ActionPlayVoice 播放语音
	ActionPlayVoice ActionCode = "1001"
	// ActionLua 执行脚本
	ActionLua ActionCode = "1200"

	// ActionUser 本地分机
	ActionUser ActionCode = "2000"
	// ActionIvr IVR
	ActionIvr ActionCode = "2001"
	// ActionCallcenterQueue 呼叫中心队列
	ActionCallcenterQueue ActionCode = "2003"

	// ActionGateway 转接外线
	ActionGateway ActionCode = "3000"
)

const (
	// CompletionCauseUriFailure 文件写失败
	CompletionCauseUriFailure RecordCompletionCause = "uri-failure"
	// CompletionCauseEmptyFile 空文件
	CompletionCauseEmptyFile RecordCompletionCause = "empty-file"
	// CompletionCauseInputTooShort 文件流太短
	CompletionCauseInputTooShort RecordCompletionCause = "input-too-short"
	// CompletionCauseSuccessSilence 成功
	CompletionCauseSuccessSilence RecordCompletionCause = "success-silence"
	// CompletionCauseNoInputTimeout 语音流检测超时
	CompletionCauseNoInputTimeout RecordCompletionCause = "no-input-timeout"
	// CompletionCauseSuccessMaxTime 达到最大通话时长
	CompletionCauseSuccessMaxTime RecordCompletionCause = "success-maxtime"
)
