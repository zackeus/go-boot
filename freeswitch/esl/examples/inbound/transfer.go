package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-zero/core/logx"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.71:8021")
	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	defer inbound.Close()

	ctx := context.Background()

	uuid := "06ae07e8-a68d-4981-966a-4b51308080ca"
	bLeg := esl.Leg{CallURL: "1003 XML outbound"}
	if err := inbound.Transfer(ctx, uuid, bLeg); err != nil {
		logx.Error(err)
	} else {
		logx.Error("ok *******************")
	}
}
