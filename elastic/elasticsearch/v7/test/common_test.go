package test

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/elastic/elasticsearch/v7/esutil"
	"gitee.com/zackeus/go-boot/elastic/elasticsearch/v7/helper"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestPing(t *testing.T) {
	c, err := initESCLi()
	assert.Empty(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	res, err := c.Ping(
		c.Ping.WithContext(ctx),
		c.Ping.WithErrorTrace(),
	)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(res)
	}
}

func TestInfo(t *testing.T) {
	c, err := initESCLi()
	assert.Empty(t, err)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	res, err := c.Info(
		c.Info.WithContext(ctx),
		c.Info.WithErrorTrace(),
	)
	assert.Empty(t, err)
	t.Log(res.String())
}

// 创建索引
func TestCreateIndex(t *testing.T) {
	c, err := initESCLi()
	assert.Empty(t, err)

	/* number_of_shards：每个索引的主分片数 */
	/* number_of_replicas：每个主分片的副本数 */
	/* text: 会拆词 keyword: 不会拆词 */
	/* ignore_above: 超过字段不会存储索引 */
	/* analyzer: 设置默认分词器 */
	/* 结构: id name */
	// const json = `{"settings":{"number_of_shards":1,"number_of_replicas":0,"analysis":{"analyzer":{"default":{"type":"ik_max_word"}}}},"mappings":{"properties":{"id":{"type": "long"}, "name":{"type": "text"}}}}`
	// const json = `{"settings":{"number_of_shards":1,"number_of_replicas":0},"mappings":{"properties":{"id":{"type": "long"}, "name":{"type": "text","fields":{"keyword":{"type":"keyword","ignore_above":100}}}}}}`
	// const json = `{"settings":{"number_of_shards":1,"number_of_replicas":0},"mappings":{"properties":{"id":{"type": "long"}, "name":{"type": "keyword","ignore_above":64}}}}`

	/* 使用模版索引 */
	/* 创建第一个索引 test-{now/d}-000001，设置索引别名为 test，后续在 rollover 滚动更新索引时，索引名会根据最后的序号递增 */
	/* is_write_index 参数设置为 true 表示往别名发送的写请求将发送到 test-000001 索引上。当发生 Rollover 时，Elasticsearch 会自动将新创建的索引的 is_write_index 参数设置为 true，同时将旧索引的 is_write_index 参数设置为 false，以确保往别名写入时只写入同一个索引 */
	const json = `{"aliases":{"test":{"is_write_index":true}}}`
	res, err := c.Indices.Create(url.QueryEscape("<test-{now/d}-000001>"), c.Indices.Create.WithContext(context.Background()), c.Indices.Create.WithBody(strings.NewReader(json)))

	assert.Empty(t, err)
	t.Log(res.String())
}

// 删除索引
func TestDeleteIndex(t *testing.T) {
	c, err := initESCLi()
	assert.Empty(t, err)

	res, err := c.Indices.Delete([]string{index}, c.Indices.Delete.WithContext(context.Background()))
	assert.Empty(t, err)

	if res.IsError() {
		t.Error(res.String())
	} else {
		t.Log(res.String())
	}
}

// 创建索引模版
func TestCreateIndexTemplate(t *testing.T) {
	c, err := initESCLi()
	assert.Empty(t, err)

	/* index_patterns: 必须的参数，使用通配符定义匹配索引的规则 */
	/* priority: 可选的参数，索引模版的匹配优先级，如果不填默认0（最低优先级），多个模版时，优先匹配优先级高的模版 */
	/* refresh_interval: 索引刷新时间 */
	/* lifecycle.name: 绑定的索引周期 */
	/* lifecycle.rollover_alias: rollover后的索引别名 */
	/* text: 会拆词 keyword: 不会拆词 */
	/* ignore_above: 超过字段不会存储索引 */
	/* analyzer: 设置默认分词器 */
	/* 结构: id name */
	const json = `{"index_patterns":"test-*","priority":100,{"template":{"settings":{"index":{"lifecycle":{"name":"test-ilm-policy","rollover_alias":"test"},"refresh_interval":"1s","analysis":{"analyzer":{"default":{"type":"ik_max_word"}}},"number_of_shards":"1","number_of_replicas":"1"}},"mappings":{"properties":{"id":{"type":"long"},"name":{"type":"text"}}}}}}`
	res, err := c.Indices.PutIndexTemplate(index, strings.NewReader(json),
		c.Indices.PutIndexTemplate.WithContext(context.Background()),
		c.Indices.PutIndexTemplate.WithCreate(true),
	)
	assert.Empty(t, err)
	t.Log(res.String())
}

// 创建更新文档
func TestSaveDocument(t *testing.T) {
	c, err := initESCLi()
	assert.Empty(t, err)

	const data1 = `{"id":1,"name":"张舟"}`
	const data2 = `{"id":2,"name":"张胜松"}`
	const data3 = `{"id":3,"name":"我是中国人"}`
	ctx := context.Background()

	res1, err := c.Create(index, "100", strings.NewReader(data1),
		c.Create.WithContext(ctx),
	)

	switch {
	case err == nil:
		t.Log(res1)
	case helper.IsStatusCode(err, http.StatusConflict):
		/* 409 重复数据 */
		t.Error("重复数据***************")
		t.Error(err)
	default:
		t.Error(err)
	}

	//res2, err := c.Index(index, strings.NewReader(data2),
	//	c.Index.WithContext(ctx),
	//	c.Index.WithDocumentID("101"), // 指定文档ID
	//)
	//assert.Empty(t, err)
	//if res2.IsError() {
	//	t.Error(res2.String())
	//} else {
	//	t.Log(res2.String())
	//}
	//
	//res3, err := c.Index(index, strings.NewReader(data3),
	//	c.Index.WithContext(ctx),
	//	c.Index.WithDocumentID("102"), // 指定文档ID
	//)
	//assert.Empty(t, err)
	//if res3.IsError() {
	//	t.Error(res3.String())
	//} else {
	//	t.Log(res3.String())
	//}
}

// 测试批量操作
func TestBulk(t *testing.T) {
	c, err := initESCLi()
	assert.Empty(t, err)

	gCtx, cancel := context.WithCancel(context.Background())
	defer cancel()

	indexer, err := esutil.NewBulkIndexer(esutil.BulkIndexerConfig{
		Client:        c,
		NumWorkers:    5,
		FlushInterval: time.Second * 5,
		ErrorTrace:    true,
		OnFlushStart: func(ctx context.Context) context.Context {
			t.Log("OnFlushStart *****************")
			return gCtx
		},
		OnFlushEnd: func(ctx context.Context) {
			t.Log("OnFlushEnd *****************")
		},
		OnError: func(ctx context.Context, err error) {
			t.Error("onError: ", err)
		},
	})
	assert.Empty(t, err)

	/* delete: 删除 */
	/* create: 创建 如果需要创建的文档已经存在，那么创建失败 */
	/* index: 创建或替换 如果要创建的文档不存在则执行创建操作，如果已经存在则执行替换操作 */
	/* update: 更新 */
	data := []string{`{"id":1,"name":"张舟"}`, `{"id":2,"name":"张胜松11"}`, `{"id":3,"name":"我是中国人"}`}
	for n, i := range data {
		err = indexer.Add(gCtx, esutil.BulkIndexerItem{
			Action:     "create",
			Index:      fmt.Sprintf("test-202%d", n),
			DocumentID: strconv.Itoa(n),
			Body:       strings.NewReader(i),
			OnSuccess: func(ctx context.Context, item esutil.BulkIndexerItem, item2 esutil.BulkIndexerResponseItem) {
				t.Log("OnSuccess: ", item, " : ", item2)
			},
			OnFailure: func(ctx context.Context, item esutil.BulkIndexerItem, item2 esutil.BulkIndexerResponseItem, err error) {
				t.Error("OnFailure: ", item, " : ", item2, " err: ", err)
			},
		})
		assert.Empty(t, err)
	}

	err = indexer.Close(gCtx)
	assert.Empty(t, err)

	/* 统计信息 */
	stats := indexer.Stats()
	t.Logf("NumAdded: %d, NumFlushed: %d. NumFailed: %d", stats.NumAdded, stats.NumFlushed, stats.NumFailed)
}

// 查询
func TestSearch(t *testing.T) {
	c, err := initESCLi()
	assert.Empty(t, err)

	/* Term: 精确匹配 */
	/* Match: 分词模糊匹配 */
	/* Wildcard: 通配符匹配 */
	/* Range: 范围查询 */
	/* must: and 且 */
	/* should: or 或 */
	/* must_not: 或取反 */
	/* filter: 类似 must 但不评分 效率略高 */
	//const aa = `{"query":{"match":{"name":"胜松"}}}`
	//const aa = `{"query":{"bool":{"must":{"term":{"name":"中国"}}}}}`
	//const aa = `{"query":{"bool":{"must":{"match":{"name":"胜松"}}}}}`
	const aa = `{"query":{"bool":{"filter":{"match":{"switchName":"baic"}}}}}`
	//const aa = `{"query":{"bool":{"must":{"wildcard":{"name":"张*"}}}}}`
	res, err := c.Search(
		c.Search.WithContext(context.Background()),
		c.Search.WithIndex("cdr"),
		c.Search.WithBody(strings.NewReader(aa)),
		//c.Search.WithQuery(aa),
		c.Search.WithTrackTotalHits(true), // 统计总文档数(会损耗性能)
		c.Search.WithPretty(),             // 响应格式化
	)
	if err != nil {
		t.Error(err)
		return
	}

	if res.Hits == nil || res.Hits.Hits == nil || len(res.Hits.Hits) == 0 {
		return
	}
	t.Log("total: ", res.Hits.TotalHits.Value)
	for _, hit := range res.Hits.Hits {
		t.Log(hit)
	}
}
