package cron

import (
	"gitee.com/zackeus/go-boot/cron/core"
	"gitee.com/zackeus/go-boot/cron/drive"
	"time"
)

// Option is Cron Option
type Option func(*Cron)

func WithDriver(d drive.Driver) Option {
	return func(cron *Cron) {
		cron.driver = d
		cron.events = make(chan *drive.Event, 10)
	}
}

// WithHashReplicas set hashReplicas
func WithHashReplicas(d int) Option {
	return func(cron *Cron) {
		cron.hashReplicas = d
	}
}

// WithCronLocation is warp cron with location
func WithCronLocation(loc *time.Location) Option {
	return func(cron *Cron) {
		f := core.WithLocation(loc)
		cron.crOptions = append(cron.crOptions, f)
	}
}

// WithCronSeconds is warp cron with seconds
func WithCronSeconds() Option {
	return func(cron *Cron) {
		f := core.WithSeconds()
		cron.crOptions = append(cron.crOptions, f)
	}
}

// WithCronParser is warp cron with schedules.
func WithCronParser(p core.ScheduleParser) Option {
	return func(cron *Cron) {
		f := core.WithParser(p)
		cron.crOptions = append(cron.crOptions, f)
	}
}

// WithCronChain is Warp cron with chain
func WithCronChain(wrappers ...core.JobWrapper) Option {
	return func(cron *Cron) {
		f := core.WithChain(wrappers...)
		cron.crOptions = append(cron.crOptions, f)
	}
}

// WithRecoverFunc You can defined yourself recover function to make the
// job will be added to your dcron when the process restart
//func WithRecoverFunc(recoverFunc RecoverFuncType) Option {
//	return func(cron *Cron) {
//		cron.RecoverFunc = recoverFunc
//	}
//}

// WithClusterStable 设置集群不可用下调度失败的job 在指定时间窗口内集群恢复后可继续执行
//func WithClusterStable(timeWindow time.Duration) Option {
//	return func(cron *Cron) {
//		cron.recentJobs = NewRecentJobPacker(timeWindow)
//	}
//}

func RunningLocally() Option {
	return func(cron *Cron) {
		cron.runningLocally = true
	}
}
