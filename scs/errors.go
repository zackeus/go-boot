package scs

import (
	"errors"
)

var (
	NoSessionData   = errors.New("scs: no session data in context")
	SessionNotFound = errors.New("scs: session not found")
)
