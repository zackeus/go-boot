package stringx

import (
	"strings"
	"sync"
	"text/template"
)

var tp *template.Template
var mu sync.Mutex

func init() {
	tp = template.New("")
}

// Format 自定义命名format，严格按照 {{.CUSTOMNAME}} 作为预定参数，不要写任何其它的template语法
func Format(fmt string, args map[string]any) string {
	mu.Lock()
	defer mu.Unlock()

	temp, err := tp.Parse(fmt)
	if err != nil {
		return fmt
	}

	s := new(strings.Builder)
	if err := temp.Execute(s, args); err != nil {
		return fmt
	}
	return s.String()
}
