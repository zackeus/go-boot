package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-zero/core/logx"
	"os"
	"time"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.71:8021")
	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	sig := make(chan os.Signal)

	// Create a basic context
	//ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	//defer cancel()
	ctx := context.Background()

	// Place the call in the background(bgapi) to user 100 and playback an audio file as the bLeg and no exported variables
	aLeg := esl.Leg{CallURL: "user/1002", LegVariables: map[string]string{
		freeswitch.VarLegTimeout:                "10",
		freeswitch.VarCallTimeout:               "10",
		freeswitch.VarOriginationCallerIdName:   "裕隆",
		freeswitch.VarOriginationCallerIdNumber: "500100",
		freeswitch.VarEffectiveCallerIdName:     "张舟",
		freeswitch.VarEffectiveCallerIdNumber:   "15058041631",
		freeswitch.VarIgnoreEarlyMedia:          "false",
		freeswitch.VarEnableSpeechAsr:           "false",
		freeswitch.VarCcSide:                    "originate",
	}}
	bLeg := esl.Leg{CallURL: "1000 XML outbound"}
	if uuid, err := inbound.OriginateCallSync(ctx, aLeg, bLeg, map[string]string{}, 33*time.Second); err != nil {
		logx.Error(err)
	} else {
		logx.Info("ok: ", uuid)
	}

	<-sig
	inbound.Close()
}
