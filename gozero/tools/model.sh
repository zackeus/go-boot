#!/usr/bin/env bash
# go-zero model 生成脚本

# 使用方法：
# sh ./model.sh ${dbName} ${tableName}
# 再将./genModel下的文件剪切到对应服务的model目录里面，记得改package

# -c: 使用缓存
goctl model mysql ddl --src="./$1/$2.sql" --dir=./ --database="$1" --style=gozero -c --home="/Users/zackeus/.goctl"