package validator

import (
	"gitee.com/zackeus/goutil"
	"gitee.com/zackeus/goutil/reflects"
	"gitee.com/zackeus/goutil/strutil"
	"github.com/go-playground/validator/v10"
	"regexp"
)

const (
	regexChoiceSingleDigit = "^(([0-9])-([0-9]))$"
)

var dtmfVars map[string]string

func init() {
	dtmfVars = make(map[string]string, 12)
	dtmfVars["0"] = "0"
	dtmfVars["1"] = "1"
	dtmfVars["2"] = "2"
	dtmfVars["3"] = "3"
	dtmfVars["4"] = "4"
	dtmfVars["5"] = "5"
	dtmfVars["6"] = "6"
	dtmfVars["7"] = "7"
	dtmfVars["8"] = "8"
	dtmfVars["9"] = "9"
	dtmfVars["*"] = "*"
	dtmfVars["#"] = "#"
}

var (
	regexpChoiceSingleDigit = regexp.MustCompile(regexChoiceSingleDigit)
)

// 个位数选择范围
func validationChoiceSingleDigit(fl validator.FieldLevel) bool {
	return regexpChoiceSingleDigit.MatchString(fl.Field().String())
}

// 校验 field 长度位数
func validationFieldLength(fl validator.FieldLevel) bool {
	l, err := strutil.Int(fl.Param())
	if err != nil {
		panic(err)
	}

	return goutil.IsEqual(reflects.Len(fl.Field()), l)
}

// 校验 DTMF
func validationDTMF(fl validator.FieldLevel) bool {
	k := fl.Field().String()
	if len(k) != 1 {
		return false
	}

	_, ok := dtmfVars[k]
	return ok
}
