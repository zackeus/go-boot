package test

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/minio/sha256-simd"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"os"
	"testing"
)

const (
	endpoint        string = "minio.yulon.com"
	accessKeyID     string = "yulon"
	secretAccessKey string = "yulon123"
	// 使用https
	useSSL bool = true
	// ca 证书
	crt = "/Users/zackeus/golang/workspaces/cti/certs.d/dev/minio/ca.crt"
)

func initCli() (*minio.Client, error) {
	// 初始化证书池
	pool := x509.NewCertPool()
	caCrt, err := os.ReadFile(crt)
	if err != nil {
		return nil, err
	}
	// 解析证书，并添加到证书池
	pool.AppendCertsFromPEM(caCrt)

	transport := &http.Transport{
		TLSClientConfig: &tls.Config{
			RootCAs:            pool,  // 设置双向认证的证书
			InsecureSkipVerify: false, // 是否验证服务端证书，True为不验证
		},
	}

	return minio.New(endpoint, &minio.Options{
		Creds:     credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure:    useSSL,
		Transport: transport,
	})
}

// 查看bucket列表
func TestListBucket(t *testing.T) {
	cli, err := initCli()
	assert.Empty(t, err)

	buckets, err := cli.ListBuckets(context.Background())
	assert.Empty(t, err)
	for _, bucket := range buckets {
		t.Log(bucket)
	}
}

// 对象信息
func TestStat(t *testing.T) {
	bucketName := "test"
	objectName := "document/test.pdf"

	cli, err := initCli()
	assert.Empty(t, err)
	info, err := cli.StatObject(context.Background(), bucketName, objectName, minio.StatObjectOptions{
		Checksum: true, // 返回上传时携带的校验信息
	})
	assert.Empty(t, err)
	t.Log(info)
	t.Logf("etag: %s, sha256: %s", info.ETag, info.ChecksumSHA256)

	// 获取自定义tag
	tags, err := cli.GetObjectTagging(context.Background(), bucketName, objectName, minio.GetObjectTaggingOptions{})
	t.Log(tags)
}

// 上传文件
func TestUpload(t *testing.T) {
	bucketName := "test"
	objectName := "document/ipcc2.docx"
	filePath := "/Users/zackeus/Downloads/ipcc-api北财.docx"

	cli, err := initCli()
	assert.Empty(t, err)

	// Open the referenced file.
	fileReader, err := os.Open(filePath)
	assert.Empty(t, err)
	defer func(fileReader *os.File) {
		_ = fileReader.Close()
	}(fileReader)
	// Save the file stat.
	fileStat, err := fileReader.Stat()
	assert.Empty(t, err)
	// Save the file size.
	fileSize := fileStat.Size()

	b, err := io.ReadAll(fileReader)
	assert.Empty(t, err)
	// Set SHA256 header manually
	sh256 := sha256.Sum256(b)
	// base64编码sha256
	meta := map[string]string{"x-amz-checksum-sha256": base64.StdEncoding.EncodeToString(sh256[:])}

	resp, err := cli.PutObject(context.Background(), bucketName, objectName, bytes.NewReader(b), fileSize, minio.PutObjectOptions{
		DisableMultipart:     true,  // 禁用分块上传
		DisableContentSha256: true,  // 禁用文件流sha256
		SendContentMd5:       false, // 是否发送 content-md5 标头
		UserMetadata:         meta,
		UserTags:             map[string]string{"type": "pdf"}, // 自定义tag
	})
	assert.Empty(t, err)
	t.Logf("etag: %s, res sha256: %s, meta sha256: %s", resp.ETag, resp.ChecksumSHA256, meta["x-amz-checksum-sha256"])
}

// 下载文件
func TestDownload(t *testing.T) {
	bucketName := "test"
	objectName := "document/ipcc.docx"
	filePath := "/Users/zackeus/Downloads/test.docx"

	cli, err := initCli()
	assert.Empty(t, err)
	err = cli.FGetObject(context.Background(), bucketName, objectName, filePath, minio.GetObjectOptions{})
	assert.Empty(t, err)
}

// 获取文件明细
func TestStatInfo(t *testing.T) {
	bucketName := "cti"
	objectName := "document/111.pdf"

	cli, err := initCli()
	assert.Empty(t, err)
	stat, err := cli.StatObject(context.Background(), bucketName, objectName, minio.GetObjectOptions{Checksum: false})
	assert.Empty(t, err)

	t.Log(stat.Key, stat.Size)
}

// 删除文件
func TestDelete(t *testing.T) {
	bucketName := "cti"
	objectName := "document/test.pdf"

	cli, err := initCli()
	assert.Empty(t, err)
	err = cli.RemoveObject(context.Background(), bucketName, objectName, minio.RemoveObjectOptions{})
	assert.Empty(t, err)
}
