package response

type Profile struct {
	Name    string
	State   string `json:"state"`
	Context string `json:"context"`
	Url     string `json:"url"`
}
