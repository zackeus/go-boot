package dialplan

import (
	"github.com/beevik/etree"
	"strconv"
)

type (
	BreakType string
)

const (
	BreakOnFalse BreakType = "on-false"
	BreakOnTrue  BreakType = "on-true"
	BreakAlways  BreakType = "always"
	BreakNever   BreakType = "never"
)

// CreateExtension 创建路由表项
// c: 'true': 匹配成功后继续匹配；'false': 匹配成功后停止
func CreateExtension(name string, c bool) *etree.Element {
	extension := etree.NewElement("extension")
	extension.CreateAttr("name", name)
	extension.CreateAttr("continue", strconv.FormatBool(c))
	return extension
}

// CreateCondition 创建路由条件
// -------------------------------------------------------------------
// field: 匹配条件
// - 'context': Dialplan 当前的 Context
// - 'rdnis': 被转移的号码(在呼叫转移中设置)
// - 'destination_number': 被叫号码
// - 'dialplan': Dialplan 模块名字 如：XML.YAML、inline、asterisk、enum等
// - 'caller_id_name': 主叫名称
// - 'caller_id_number': 主叫号码
// - 'ani': 主叫的自动号码识别
// - 'aniii': 主叫类型
// - 'uuid': 本channel唯一标识
// - 'source': 呼叫源，来自FS哪一个模块 如 mod_sofi等
// - 'chan_name': channel 名字
// - 'network_addr': 主叫IP地址
// - 'year': 当前的年 0-9999
// - 'yday': 一年中第几天 1-366
// - 'mon': 月 1-12
// - 'mday': 日 1-31
// - 'week': 一年中第几周 1-53
// - 'mweek': 本月第几周 1-6
// - 'wday': 一周中第几天 1-7
// - 'hour': 小时 0-23
// - 'minute': 分 0-59
// - 'minute-of-day': 一天中第几分钟 1-1440 午夜=1,1点=60，中午=720
// -------------------------------------------------------------------
// expression: 表达式
// -------------------------------------------------------------------
// b:
// - 'on-false': 第一次匹配失败时停止（但继续处理其他extension），相当于A and B
// - 'on-true': 第一次匹配成功时停止（但会先完成对应的Action，然后继续处理其他的extension）不成功则继续，相当于((notA) and B)
// - 'always': 不管是否匹配，都停止
// - 'never': 不管是否匹配，都继续
func CreateCondition(field string, expression string, b ...BreakType) *etree.Element {
	if b == nil {
		b = append(b, BreakOnFalse)
	}

	condition := etree.NewElement("condition")
	condition.CreateAttr("field", field)
	condition.CreateAttr("expression", expression)
	condition.CreateAttr("break", string(b[0]))
	return condition
}
