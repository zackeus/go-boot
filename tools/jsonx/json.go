package jsonx

import (
	"gitee.com/zackeus/go-zero/core/jsonx"
)

func UnmarshalFromString(str string, v interface{}) error {
	return jsonx.UnmarshalFromString(str, v)
}

func Unmarshal(data []byte, v interface{}) error {
	return jsonx.Unmarshal(data, v)
}

func Marshal(v interface{}) ([]byte, error) {
	return jsonx.Marshal(v)
}

func MarshalToString(v interface{}) (string, error) {
	return jsonx.MarshalToString(v)
}
