// Copyright 2012-present Oliver Eilhard. All rights reserved.
// Use of this source code is governed by a MIT-license.
// See http://olivere.mit-license.org/license.txt for details.

package helper

import "gitee.com/zackeus/go-boot/tools/jsonx"

// TypeQuery filters documents matching the provided document / mapping type.
//
// For details, see:
// https://www.elastic.co/guide/en/elasticsearch/reference/7.0/query-dsl-type-query.html
type TypeQuery struct {
	typ string
}

func NewTypeQuery(typ string) *TypeQuery {
	return &TypeQuery{typ: typ}
}

// Source returns JSON for the query.
func (q *TypeQuery) Source() (interface{}, error) {
	source := make(map[string]interface{})
	params := make(map[string]interface{})
	source["type"] = params
	params["value"] = q.typ
	return source, nil
}

func (q *TypeQuery) Build() (string, error) {
	source, err := q.Source()
	if err != nil {
		return "", err
	}

	qs := make(map[string]interface{})
	qs["query"] = source

	s, err := jsonx.MarshalToString(qs)
	if err != nil {
		return "", err
	}
	return s, nil
}
