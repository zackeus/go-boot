package jsonx

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type People struct {
	StrBool bool  `json:"strBool"`
	NumBool bool  `json:"numBool"`
	StrNum  int64 `json:"strNum"`
}

func TestUnmarshal(t *testing.T) {
	p := &People{}
	err := Unmarshal([]byte(`{"strBool": "FALSE", "numBool": 1, "strNum": "99"}`), p)
	assert.Nil(t, err)
	t.Log(p)
}
