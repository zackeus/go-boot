package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-zero/core/logx"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.90:8021")
	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	defer inbound.Close()

	ctx := context.Background()

	val, err := inbound.FindCallCenterQueues(ctx)
	if err != nil {
		logx.Error(err)
		return
	}

	for _, q := range val {
		logx.Info(q)
	}

}
