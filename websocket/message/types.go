package message

import "github.com/gorilla/websocket"

type (
	// Type 事件类型
	Type int
)

const (
	// TypeText 表示文本数据消息。文本消息负载被解释为 UTF-8 编码的文本数据
	TypeText Type = websocket.TextMessage
	// TypeBinary 二进制数据消息
	TypeBinary Type = websocket.BinaryMessage
	// TypeClose 表示关闭控制消息。可选消息负载包含数字代码和文本
	TypeClose Type = websocket.CloseMessage
	// TypePing 表示 ping 控制消息。可选的消息负载是 UTF-8 编码的文本
	TypePing Type = websocket.PingMessage
	// TypePong 表示一个 pong 控制消息。可选的消息负载是 UTF-8 编码的文本
	TypePong Type = websocket.PongMessage
)
