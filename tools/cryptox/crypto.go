package cryptox

import (
	"crypto/rand"
	"encoding/base64"
)

// Salt 生成指定位数随机 salt
func Salt(length int) (string, error) {
	salt := make([]byte, length)
	_, err := rand.Read(salt)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(salt)[:length], nil
}
