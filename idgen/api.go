package idgen

import (
	"time"
)

type (
	IdGenerator interface {
		Available() bool
		NextId() (uint64, error)
		ExtractTime(id uint64) time.Time
		Shutdown(g ...bool)
	}
)
