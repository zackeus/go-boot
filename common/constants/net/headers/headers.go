package headers

const (
	Vary            = "Vary"
	SetCookie       = "Set-Cookie"
	CacheControl    = "Cache-Control"
	AcceptLanguage  = "Accept-Language"
	SecWebSocketKey = "Sec-WebSocket-Key"

	XRealIP = "X-Real-IP"

	XForwardedFor   = "X-Forwarded-For"
	XForwardedHost  = "X-Forwarded-Host"
	XForwardedProto = "X-Forwarded-Proto"

	// XClientToken 客户端自定义标识
	XClientToken = "X-Client-Token"

	Accept = "Accept"
	// AcceptRanges 标识支持的范围请求
	AcceptRanges = "Accept-Ranges"

	ContentDisposition = "Content-Disposition"
	// ContentType is the header key for Content-Type.
	ContentType   = "Content-Type"
	ContentLength = "Content-Length"

	ApplicationSdp  = "application/sdp"
	ApplicationJson = "application/json"
	// ApplicationJsonUtf8 is the content type for JSON.
	ApplicationJsonUtf8 = "application/json; charset=utf-8"
	ApplicationFormUtf8 = "application/x-www-form-urlencoded"
	ApplicationXml      = "application/xml"
	ApplicationStream   = "application/octet-stream"
	Filename            = "attachment;filename="
)
