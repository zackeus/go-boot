package test

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-zero/core/logx"
	"gitee.com/zackeus/goutil"
	"github.com/stretchr/testify/assert"
	"go.etcd.io/etcd/client/pkg/v3/transport"
	clientv3 "go.etcd.io/etcd/client/v3"
	"testing"
	"time"
)

var hosts = []string{"etcd.node1.server:2379"}
var caCertFile = "/Users/zackeus/golang/workspaces/xswitch/certs.d/dev/etcd/ca.pem"
var certFile = "/Users/zackeus/golang/workspaces/xswitch/certs.d/dev/etcd/client.pem"
var certKeyFile = "/Users/zackeus/golang/workspaces/xswitch/certs.d/dev/etcd/client.key"

func initEtcdCli() *clientv3.Client {
	/* 证书 */
	tlsInfo := transport.TLSInfo{
		TrustedCAFile: caCertFile,
		CertFile:      certFile,
		KeyFile:       certKeyFile,
	}
	tlsConfig, err := tlsInfo.ClientConfig()
	goutil.PanicIfErr(err)

	/* 连接实例化 */
	cli, err := clientv3.New(clientv3.Config{
		Endpoints:   hosts,
		TLS:         tlsConfig,
		DialTimeout: 5 * time.Second,
	})
	goutil.PanicIfErr(err)
	return cli
}

func TestEtcdGet(t *testing.T) {
	cli := initEtcdCli()
	defer func(cli *clientv3.Client) {
		_ = cli.Close()
	}(cli)

	ctx := context.Background()

	key := "xswitch/"
	res, err := cli.Get(ctx, key, clientv3.WithPrefix())
	assert.Empty(t, err)

	t.Log("len: ", len(res.Kvs))
	for _, kv := range res.Kvs {
		t.Log(string(kv.Key), " : ", string(kv.Value))
	}
}

func TestEtcdLease(t *testing.T) {
	cli := initEtcdCli()
	defer func(cli *clientv3.Client) {
		_ = cli.Close()
	}(cli)

	key := "xswitch/test/100"

	ctx := context.Background()
	lease := clientv3.NewLease(cli)
	/* 声明一个 10s ttl 的租约 */
	leaseGrant, err := lease.Grant(ctx, 10)
	t.Error("id: ", leaseGrant.ID)
	assert.Empty(t, err)
	/* 设置 key value 并且绑定租约 */
	_, err = cli.Put(ctx, key, "0", clientv3.WithLease(leaseGrant.ID))
	assert.Empty(t, err)

	/* 自动续期 */
	keepRespChan, err := lease.KeepAlive(ctx, leaseGrant.ID)
	assert.Empty(t, err)

	go func() {
		//查看续期情况 非必需，帮助观察续租的过程
		for {
			select {
			case resp := <-keepRespChan:
				if resp == nil {
					fmt.Println("租约失效")
					return
				} else {
					fmt.Println("租约成功", resp)
				}
			}
		}
	}()

	for {
		/* 持续检测key是否过期 */
		values, err := cli.Get(ctx, key)
		if err != nil {
			t.Error(err)
			time.Sleep(time.Second * 3)
			continue
		}

		if values.Count == 0 {
			fmt.Println("已经过期")
		} else {
			fmt.Println("没过期", values.Kvs)
		}
		time.Sleep(time.Second * 3)
	}
}

func TestEtcdPrefix(t *testing.T) {
	cli := initEtcdCli()
	defer func(cli *clientv3.Client) {
		_ = cli.Close()
	}(cli)

	prefix := "cti/"
	ctx := context.Background()
	resp, err := cli.Get(ctx, prefix, clientv3.WithPrefix())
	assert.Empty(t, err)

	for _, ev := range resp.Kvs {
		t.Log(fmt.Sprintf("%s:%s ", ev.Key, ev.Value))
	}
}

// 获取 key 的 version
func TestEtcdKeyVersion(t *testing.T) {
	cli := initEtcdCli()
	defer func(cli *clientv3.Client) {
		_ = cli.Close()
	}(cli)

	ctx := context.Background()
	key := "cti/id/worker/ippbx/global"

	/* 定义事务 先put 再 get */
	tx := cli.Txn(ctx).Then(clientv3.OpPut(key, "0"), clientv3.OpGet(key))
	// 执行事务
	resp, err := tx.Commit()
	assert.Empty(t, err)
	if resp.Succeeded {
		/* 获取 version 值 */
		logx.Info("version: ", resp.Responses[1].GetResponseRange().GetKvs()[0].Version)
	}

}

func TestEtcdWatch(t *testing.T) {
	cli := initEtcdCli()
	defer func(cli *clientv3.Client) {
		_ = cli.Close()
	}(cli)

	key := "xswitch/test"
	ctx, cancel := context.WithCancel(clientv3.WithRequireLeader(context.Background()))
	defer cancel()
	/* 启用 PrevKV */

	rch := cli.Watch(ctx, key, clientv3.WithPrevKV(), clientv3.WithPrefix())

	for {
		select {
		case resp := <-rch:
			if resp.Err() != nil {
				t.Error(resp.Err())
				return
			}

			for _, ev := range resp.Events {
				fmt.Printf("%s \n", ev.Type)
				if ev.PrevKv != nil {
					fmt.Printf("old【%q:%q:%d】\n", ev.PrevKv.Key, ev.PrevKv.Value, ev.PrevKv.Lease)
				}
				if ev.Kv != nil {
					fmt.Printf("now【%q:%q:%d】\n", ev.Kv.Key, ev.Kv.Value, ev.Kv.Lease)
				}
			}
		case <-ctx.Done():
			fmt.Println("Watch closed")
			return
		}
	}
}

// 事务
func TestEtcdTx(t *testing.T) {
	cli := initEtcdCli()
	defer func(cli *clientv3.Client) {
		_ = cli.Close()
	}(cli)

	ctx := context.Background()
	key := "cti/id/worker/ippbx/global"
	/* 判断 key 是否存在租约 没有则更新值 */
	resp, err := cli.Txn(ctx).
		If(clientv3.Compare(clientv3.LeaseValue("cti/id/worker/ippbx/0"), "=", 0)).
		Then(clientv3.OpPut(key, "0")).
		Commit()

	assert.Empty(t, err)
	t.Log("success: ", resp.Succeeded)
}
