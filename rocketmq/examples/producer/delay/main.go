package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/rocketmq"
	"gitee.com/zackeus/go-boot/rocketmq/rlog"
	"gitee.com/zackeus/go-zero/core/logx"
	"os"

	"gitee.com/zackeus/go-boot/rocketmq/primitive"
	"gitee.com/zackeus/go-boot/rocketmq/producer"
)

const (
	Topic     = "test-delay-topic"
	Endpoint  = "192.168.137.26:9876"
	AccessKey = ""
	SecretKey = ""
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}
	rlog.SetLevel("error")

	nameservers := []string{Endpoint}
	/* 消息追踪 */
	traceCfg := &primitive.TraceConfig{
		Access:   primitive.Local,
		Resolver: primitive.NewPassthroughResolver(nameservers),
	}

	p, _ := rocketmq.NewProducer(
		producer.WithNsResolver(primitive.NewPassthroughResolver(nameservers)),
		producer.WithTrace(traceCfg),
	)
	if err := p.Start(); err != nil {
		fmt.Printf("start producer error: %s", err.Error())
		os.Exit(1)
	}

	for i := 0; i < 5; i++ {
		level := i + 1
		msg := primitive.NewMessage(Topic, []byte("Hello RocketMQ Go Client! "+string(rune(level))))
		msg.WithDelayTimeLevel(level)
		res, err := p.SendSync(context.Background(), msg)

		if err != nil {
			fmt.Printf("send message error: %s\n", err)
		} else {
			fmt.Printf("send message success: result=%s\n", res.String())
		}
	}

	if err := p.Shutdown(); err != nil {
		fmt.Printf("shutdown producer error: %s", err.Error())
	}
}
