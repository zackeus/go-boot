package sep

import "gitee.com/zackeus/goutil/comdef"

const (
	// SepSemicolon 封号
	SepSemicolon string = comdef.SemicolonStr
	// SepComma 逗号
	SepComma string = comdef.CommaStr
	// SepVerticalBar 竖线
	SepVerticalBar string = "|"
)
