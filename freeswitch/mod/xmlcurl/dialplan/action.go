package dialplan

import (
	"github.com/beevik/etree"
	"strconv"
)

type (
	ActionOption func(e *etree.Element)
)

// CreateAction 创建执行动作
func CreateAction(application string, data string, opts ...ActionOption) *etree.Element {
	action := etree.NewElement("action")
	action.CreateAttr("application", application)
	action.CreateAttr("data", data)

	for _, opt := range opts {
		opt(action)
	}
	return action
}

// WithAnti 是否为反动作
func WithAnti(anti bool) ActionOption {
	return func(action *etree.Element) {
		if anti {
			action.Tag = "anti-action"
		} else {
			action.Tag = "action"
		}
	}
}

// WithInline 是否为内联动作
func WithInline(inline bool) ActionOption {
	return func(action *etree.Element) {
		if inline {
			action.CreateAttr("inline", strconv.FormatBool(inline))
		} else {
			action.RemoveAttr("inline")
		}
	}
}
