package command

type Exit struct{}

func (Exit) BuildCmd() string {
	return "exit"
}

func (Exit) BuildMessage() string {
	return "exit"
}
