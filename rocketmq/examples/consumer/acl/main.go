package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/rocketmq"
	"gitee.com/zackeus/go-boot/rocketmq/consumer"
	"gitee.com/zackeus/go-boot/rocketmq/primitive"
	"gitee.com/zackeus/go-boot/rocketmq/rlog"
	"gitee.com/zackeus/go-zero/core/logx"
	"os"
)

const (
	Topic         = "test-acl-topic"
	ConsumerGroup = "test-acl-group"
	Endpoint      = "192.168.137.26:9876"
	AccessKey     = "testuser"
	SecretKey     = "yulon123"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}
	rlog.SetLevel("error")

	nameservers := []string{Endpoint}
	/* 消息轨迹追踪 */
	traceCfg := &primitive.TraceConfig{
		Access:   primitive.Local,
		Resolver: primitive.NewPassthroughResolver(nameservers),
	}

	sig := make(chan os.Signal)
	c, err := rocketmq.NewPushConsumer(
		consumer.WithGroupName(ConsumerGroup),
		consumer.WithNsResolver(primitive.NewPassthroughResolver([]string{Endpoint})),
		consumer.WithTrace(traceCfg),
		/* 鉴权消息 */
		consumer.WithCredentials(primitive.Credentials{
			AccessKey: AccessKey,
			SecretKey: SecretKey,
		}),
	)
	if err != nil {
		fmt.Println("init consumer error: " + err.Error())
		os.Exit(0)
	}

	if err = c.Subscribe(Topic, consumer.MessageSelector{}, func(ctx context.Context, msgs ...*primitive.MessageExt) (consumer.ConsumeResult, error) {
		fmt.Printf("subscribe callback: %v \n", msgs)
		return consumer.ConsumeSuccess, nil
	}); err != nil {
		fmt.Println(err.Error())
	}

	// Note: start after subscribe
	if err := c.Start(); err != nil {
		fmt.Println(err.Error())
		os.Exit(-1)
	}

	<-sig
	if err := c.Shutdown(); err != nil {
		fmt.Printf("shutdown Consumer error: %s", err.Error())
	}
}
