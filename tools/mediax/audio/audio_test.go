package audio

import (
	"testing"
)

func TestProbe(t *testing.T) {
	path := "/Users/zackeus/Downloads/test.wav"
	t.Run("audio-probe", func(t *testing.T) {
		got, err := Probe(path)
		if err != nil {
			t.Errorf("Probe() error = %v", err)
			return
		}
		t.Log(got)
	})
}
