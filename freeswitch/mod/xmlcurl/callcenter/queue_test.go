package callcenter

import (
	"gitee.com/zackeus/go-boot/freeswitch/mod/callcenter/queue"
	"os"
	"testing"
)

func TestNewQueue(t *testing.T) {
	x, err := NewQueue("test", queue.StrategyRandom, WithMaxWaitTime(300))
	if err != nil {
		t.Error(err)
		return
	}

	if p, ok := x.(*XmlQueue); ok {
		p.doc.Indent(1)
		_, _ = p.doc.WriteTo(os.Stdout)
	}
}
