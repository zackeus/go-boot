package etcd

import (
	"time"
)

type (
	// Options 可选项
	Options func(e *EtcdDriver)
)

// WithTimeout 超时
func WithTimeout(t time.Duration) Options {
	return func(e *EtcdDriver) {
		e.timeout = t
	}
}

// WithPrefix etcd 注册前缀
func WithPrefix(p string) Options {
	return func(e *EtcdDriver) {
		e.prefix = p
	}
}

// WithTTL etcd 租约周期
func WithTTL(n int64) Options {
	return func(e *EtcdDriver) {
		e.ttl = n
	}
}

// WithKeepAliveInterval 自动续期间隔
func WithKeepAliveInterval(t time.Duration) Options {
	return func(e *EtcdDriver) {
		e.reKeepAliveInterval = t
	}
}
