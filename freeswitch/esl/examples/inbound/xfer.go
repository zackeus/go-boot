// 例：A呼叫B，B将对话转给C，此时：
//
//	1.转接过程中，C振铃，B听回铃音，A听等待音
//	2、若C未接时，B挂断，A挂断, C振铃，此时转接放弃
//	3、若B未挂，B想取消转接，请按#号，返回A和B通话，若C不接或者忙线，则自动返回A与B通话
//	4、如果C已经接起，那么B与C通话，A听等待音，此时，任意一方挂机，另外两方通话，若B按0，那么转为三方通话

package main

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/freeswitch/esl"
	"gitee.com/zackeus/go-boot/freeswitch/esl/command"
	"gitee.com/zackeus/go-zero/core/logx"
)

func main() {
	if err := logx.SetUp(logx.LogConf{Mode: "console", Encoding: "plain"}); err != nil {
		fmt.Println(err)
		return
	}

	// Connect to FreeSWITCH
	inbound := esl.NewInbound("192.168.137.71:8021")
	if err := inbound.Start(); err != nil {
		fmt.Println("Error connecting", err)
		return
	}
	defer inbound.Close()

	ctx := context.Background()

	uuid := "e8271526-0722-4ac7-8ef1-09cedf243c20"
	bLeg := esl.Leg{CallURL: "att_xfer:1003 XML features"}
	cmd := &command.SendMessage{
		UUID:    uuid,
		Sync:    true,
		Command: "execute",
		AppName: "execute_extension",
		AppArg:  bLeg.String(),
	}
	if err := inbound.SendMsg(ctx, cmd); err != nil {
		logx.Error(err)
	}
}
