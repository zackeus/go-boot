package headers

const (
	ContentType        = "Content-Type"
	ContentLength      = "Content-Length"
	ContentDisposition = "Content-Disposition"
	UniqueId           = "Unique-Id"
	OtherUniqueId      = "Other-Leg-Unique-Id"
	UniqueID           = "Unique-ID"
	// CoreUUID 当前对象在FreeSWITCH内核中的UUID，关联事件本身
	CoreUUID        = "Core-UUID"
	ApplicationUUID = "Application-UUID"
	JobUUID         = "Job-UUID"
	ReplyText       = "Reply-Text"

	CallId                  = "Call-Id"
	PresenceId              = "Presence_id"
	ProfileName             = "Profile-Name"
	DomainName              = "Domain_name"
	AccountCode             = "Accountcode"
	UserName                = "Username"
	User                    = "User"
	Host                    = "Host"
	FromUser                = "From-User"
	FromHost                = "From-Host"
	Contact                 = "Contact"
	NetworkIp               = "Network-Ip"
	NetworkPort             = "Network-Port"
	UserAgent               = "User-Agent"
	UserContext             = "User_context"
	UpdateReg               = "Update-Reg"
	SipContact              = "Sip_contact"
	SipUser                 = "Sip_user"
	PingStatus              = "Ping-Status"
	Status                  = "Status"
	Phrase                  = "Phrase"
	Expires                 = "Expires"
	CallTimeout             = "Call_timeout"
	EffectiveCallerIdName   = "Effective_caller_id_name"
	EffectiveCallerIdNumber = "Effective_caller_id_number"

	SwitchName = "FreeSWITCH-Switchname"
	// SwitchInstance freeswitch 实例ID(自定义实现)
	SwitchInstance = "FreeSWITCH-Instance"

	// EventName 事件的名称，它描述事件所属的类型。也称为事件类型或类别
	EventName     = "Event-Name"
	EventSubclass = "Event-Subclass"
	// EventSequence 事件序列号(freeswitch实例内全局递增)
	EventSequence = "Event-Sequence"
	// EventDateLocal 基于系统时钟描述的事件日期/时间
	EventDateLocal = "Event-Date-Local"
	// EventDateGmt 基于GMT(也就是UTC)时间描述的事件日期/时间
	EventDateGmt = "Event-Date-GMT"
	// EventDateTimestamp 时间戳
	EventDateTimestamp = "Event-Date-Timestamp"
	// EventCallingFile 触发事件的C代码源文件名
	EventCallingFile = "Event-Calling-File"
	// EventCallingFunction 触发事件的函数名
	EventCallingFunction = "Event-Calling-Function"
	// EventCallingLineNumber 触发事件的C代码精确行数
	EventCallingLineNumber = "Event-Calling-Line-Number"

	// CallerIdNumber 主叫号码
	CallerIdNumber = "Caller-Caller-ID-Number"
	// DestinationNumber 被叫号码
	DestinationNumber = "Caller-Destination-Number"
	// ChannelCallState 呼叫状态
	ChannelCallState  = "Channel-Call-State"
	ChannelPresenceId = "Channel-Presence-ID"

	// CallDirection 呼叫方向(inbound:外呼;outbound来电)
	CallDirection = "Call-Direction"

	// RecordFilePath 录制路径
	RecordFilePath = "Record-File-Path"
	// RecordCompletionCause 录制完成原因
	RecordCompletionCause = "Record-Completion-Cause"

	// ------------------ 自定义 ----------------------------

	// VarRecordPath 录音路径
	VarRecordPath = "Variable_record_path"
	// VarRecordMinSec 录制最短时间
	VarRecordMinSec = "Variable_record_min_sec"
	// VarRecordSampleRate 录制采样率(音频)
	VarRecordSampleRate = "Variable_record_sample_rate"
	// VarRecordStereo 录制多声道
	VarRecordStereo = "Variable_record_stereo"
	// VarRecordAppend 录制文件追加
	VarRecordAppend = "Variable_record_append"
	// VarRecordBuffering 启用缓冲流
	VarRecordBuffering = "Variable_enable_file_write_buffering"
	// VarRecordHangUpOnError 录制失败挂机
	VarRecordHangUpOnError = "Variable_record_hangup_on_error"
	// VarRecordFileSize 录制文件大小
	VarRecordFileSize = "Variable_record_file_size"
	// VarRecordSeconds 录制时长
	VarRecordSeconds = "Variable_record_seconds"

	// CcAction callcenter 事件标识
	CcAction = "Cc-Action"
	// CcAgentUuid 座席端UUID
	CcAgentUuid = "Cc-Agent-Uuid"
	// CcAgent 座席名
	CcAgent = "Cc-Agent"
	// CcExt 坐席分机
	CcExt = "Variable_cc_ext"
	// CcAgentStatus 坐席状态
	CcAgentStatus = "Cc-Agent-Status"
	// CcAgentState 坐席呼叫状态
	CcAgentState = "Cc-Agent-State"
	// CcQueue 队列名称
	CcQueue = "Cc-Queue"
	// CcMemberUuid 队列成员UUID
	CcMemberUuid = "Cc-Member-Uuid"
	// CcMemberSessionUuid 队列成员 SESSION UUID
	CcMemberSessionUuid = "Cc-Member-Session-Uuid"
	// CcMemberCidName 队列成员名称
	CcMemberCidName = "Cc-Member-Cid-Name"
	// CcMemberCidNumber 队列成员号码
	CcMemberCidNumber = "Cc-Member-Cid-Number"
	// CcMemberJoinedEpoch 入队时间
	CcMemberJoinedEpoch = "Variable_cc_queue_joined_epoch"
	// CcMemberJoinedTime 入队时间(resume 事件 无此值)
	CcMemberJoinedTime = "Cc-Member-Joined-Time"
	// CcMemberLeavingTime 出队时间
	CcMemberLeavingTime = "Cc-Member-Leaving-Time"
	// CcCancelReason 取消原因
	CcCancelReason = "Cc-Cancel-Reason"
	// CcAgentCalledTime 坐席振铃时间
	CcAgentCalledTime = "Cc-Agent-Called-Time"
	// CcAgentAnsweredTime 坐席应答时间
	CcAgentAnsweredTime = "Cc-Agent-Answered-Time"
	// CcAgentTerminatedTime 坐席挂断时间
	CcAgentTerminatedTime = "Cc-Bridge-Terminated-Time"
	// CcHangupCause 挂断原因
	CcHangupCause = "Cc-Hangup-Cause"

	// SpeechType ASR语音事件类型
	SpeechType = "Speech-Type"
	// AsrCause ASR 响应结果(0:成功; 1: 无匹配结果; 2: 输入超时)
	AsrCause = "ASR-Completion-Cause"

	// IvrKey IVR 日志按键
	IvrKey = "Ivr-Key"
	// IvrType IVR 节点类型
	IvrType = "Ivr-Type"
	// IvrId IVR 节点ID
	IvrId = "Ivr-Id"
	// IvrOwnerId IVR 节点所属ID
	IvrOwnerId = "Ivr-Owner-Id"
	// IvrName IVR 节点名称
	IvrName = "Ivr-Name"
	// IvrEnterTime IVR 节点进入时间
	IvrEnterTime = "Ivr-Enter-Time"
)
