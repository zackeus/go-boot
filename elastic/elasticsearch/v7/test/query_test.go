package test

import (
	"context"
	"fmt"
	"gitee.com/zackeus/go-boot/elastic/elasticsearch/v7/helper"
	"gitee.com/zackeus/go-zero/core/logx"
	"gitee.com/zackeus/goutil/timex"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
	"time"
)

type (
	SMS struct {
		Id         int64     `db:"id" json:"id"`                  // 主键
		AppId      string    `db:"app_id" json:"appId"`           // 来源主键
		AppSys     string    `db:"app_sys" json:"appSys"`         // 来源系统
		SmsType    string    `db:"sms_type" json:"smsType"`       // 短信类型
		AppNbr     string    `db:"app_nbr" json:"appNbr"`         // 合同号
		RxTel      string    `db:"rx_tel" json:"rxTel"`           // 接收号码
		CreateDate time.Time `db:"create_date" json:"createDate"` // 创建时间
	}
)

func TestBoolQuery(t *testing.T) {
	q := helper.NewBoolQuery()
	q.Must(
		helper.NewTermQuery("id", 1),
		helper.NewMatchQuery("name", "张"),
		helper.NewWildcardQuery("test", "国家*"),
	)
	q.Filter(helper.NewRangeQuery("createDate").Gte("2021").Lte("2024"))

	fmt.Println(q.Build())
}

// 测试PIT 一般用于分页数据一致性
func TestPIT(t *testing.T) {
	c, err := initESCLi()
	if !assert.Nil(t, err) {
		return
	}

	r, err := c.OpenPointInTime([]string{"cdr"}, "1h",
		c.OpenPointInTime.WithContext(context.Background()),
		c.OpenPointInTime.WithPretty(),
	)
	if !assert.Nil(t, err) {
		logx.Error(err)
		return
	}
	t.Log(r.Id)
}

// SearchAfter 查询
func TestSearchAfter(t *testing.T) {
	bt, err := timex.FromDate("2025-01-15 00:00:00.000", timex.LayoutWithMs3)
	if !assert.Nil(t, err) {
		return
	}
	et, err := timex.FromDate("2025-01-17 00:00:00.000", timex.LayoutWithMs3)
	if !assert.Nil(t, err) {
		return
	}

	q := helper.NewBoolQuery()
	q.Filter(helper.NewRangeQuery("beginTime").Gte(bt).Lte(et))
	t.Log(q.Build())

	sq := helper.NewSearchSource().
		From(0).
		Size(3).
		Sort("beginTime", false).
		//Sort("_id", false).
		PointInTime(helper.NewPointInTimeWithKeepAlive("t9e1AwIIY2RyLTIwMjUWSU53UmVhcGNRQTJWQkRuZzduU3RrZwAWRUNuXzQzdVBUTW0xQ2JvRVY5LTNPQQAAAAAAACZJkxZqSmZERHBuV1FoNlJVbWVQQTlSM1FnAAhjZHItMjAyNBZaaTBwOE5GY1FxV1hVeFVvYjgwckRRABZFQ25fNDN1UFRNbTFDYm9FVjktM09BAAAAAAAAJkmSFmpKZkREcG5XUWg2UlVtZVBBOVIzUWcAAhZaaTBwOE5GY1FxV1hVeFVvYjgwckRRAAAWSU53UmVhcGNRQTJWQkRuZzduU3RrZwAA", "1h")). // 使用PIT 最终sort会包含 隐含的字段 tiebreaker
		SearchAfter("1737017244000", "2").                                                                                                                                                                                                                                                                                                                                                 // 使用上次查询最后文档的 sort
		Query(q)
	qb, err := sq.Build()
	if !assert.Nil(t, err) {
		return
	}
	t.Log(qb)

	c, err := initESCLi()
	if !assert.Nil(t, err) {
		return
	}

	/* 执行查询 */
	res, err := c.Search(
		c.Search.WithContext(context.Background()),
		//c.Search.WithIndex("sms"), // 如果使用 PIT 则不能这是此行
		c.Search.WithBody(strings.NewReader(qb)),
		c.Search.WithTrackTotalHits(true), // 精确统计(会损耗性能)
		c.Search.WithPretty(),             // 响应格式化
	)
	if !assert.Nil(t, err) {
		t.Error(err)
		return
	}

	t.Logf("status: %d, timeout: %v, serarchMS: %d, err: %v, total: %d，sum: %d", res.Status, res.TimedOut, res.TookInMillis, res.Error, res.TotalHits(), len(res.Hits.Hits))

	for _, hit := range res.Hits.Hits {
		t.Log(hit)
	}

	//var st SMS
	/* 数据遍历 */
	//for _, item := range res.Each(reflect.TypeOf(st)) {
	//	t.Logf("data: %v", item)
	//}
	/* 获取最后一条数据的 LastSort */
	t.Logf("sort: %v", res.LastSort())
}
