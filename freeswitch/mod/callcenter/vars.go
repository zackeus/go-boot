package callcenter

type Side string

const (
	// SideAgent 坐席端
	SideAgent Side = "agent"
	// SideMember 客户端
	SideMember Side = "member"

	// SideOriginator 发起端(自定义) 通过 api 发起呼叫
	SideOriginator Side = "originator"
)
