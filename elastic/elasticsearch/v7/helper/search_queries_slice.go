// Copyright 2012-present Oliver Eilhard. All rights reserved.
// Use of this source code is governed by a MIT-license.
// See http://olivere.mit-license.org/license.txt for details.

package helper

import "gitee.com/zackeus/go-boot/tools/jsonx"

// SliceQuery allows to partition the documents into several slices.
// It is used e.g. to slice scroll operations in Elasticsearch 5.0 or later.
// See https://www.elastic.co/guide/en/elasticsearch/reference/7.0/search-request-scroll.html#sliced-scroll
// for details.
type SliceQuery struct {
	field string
	id    *int
	max   *int
}

// NewSliceQuery creates a new SliceQuery.
func NewSliceQuery() *SliceQuery {
	return &SliceQuery{}
}

// Field is the name of the field to slice against (_uid by default).
func (q *SliceQuery) Field(field string) *SliceQuery {
	q.field = field
	return q
}

// Id is the id of the slice.
func (q *SliceQuery) Id(id int) *SliceQuery {
	q.id = &id
	return q
}

// Max is the maximum number of slices.
func (q *SliceQuery) Max(max int) *SliceQuery {
	q.max = &max
	return q
}

// Source returns the JSON body.
func (q *SliceQuery) Source() (interface{}, error) {
	m := make(map[string]interface{})
	if q.field != "" {
		m["field"] = q.field
	}
	if q.id != nil {
		m["id"] = *q.id
	}
	if q.max != nil {
		m["max"] = *q.max
	}
	return m, nil
}

func (q *SliceQuery) Build() (string, error) {
	source, err := q.Source()
	if err != nil {
		return "", err
	}

	qs := make(map[string]interface{})
	qs["query"] = source

	s, err := jsonx.MarshalToString(qs)
	if err != nil {
		return "", err
	}
	return s, nil
}
