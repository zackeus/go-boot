package filex

import (
	"os"
	"time"
)

// ModTime 获取文件的修改时间
func ModTime(path string) (time.Time, error) {
	f, err := os.Open(path)
	if err != nil {
		return time.Time{}, err
	}
	defer func(f *os.File) {
		_ = f.Close()
	}(f)

	fi, err := f.Stat()
	if err != nil {
		return time.Time{}, err
	}

	return fi.ModTime(), nil
}
