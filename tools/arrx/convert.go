package arrx

import (
	"gitee.com/zackeus/goutil"
	"gitee.com/zackeus/goutil/strutil"
	"sort"
)

type Node[T any] interface {
	ID() string
	PID() string
	GetName() string
	SetParentName(v string)
	Childs() []T
	SetChilds([]T)
}

type Nodes[E Node[E]] interface {
	// Interface 排序接口
	sort.Interface
	Lists() []E
}

// ToTree 转换树级结构
// id: 节点截取Id
func ToTree[E Node[E], L Nodes[E]](arr L, id string) []E {
	/* 排序 */
	sort.Sort(arr)

	items := make(map[string]E, len(arr.Lists())+1)
	for _, el := range arr.Lists() {
		if strutil.IsNotBlank(el.ID()) {
			/* 忽略 Id 为空字符的节点 */
			items[el.ID()] = el
		}
	}

	for _, el := range arr.Lists() {
		if _, ok := items[el.PID()]; ok {
			/* 设置父节点名称 */
			items[el.ID()].SetParentName(items[el.PID()].GetName())
			/* 父节点追加子节点 */
			items[el.PID()].SetChilds(append(items[el.PID()].Childs(), items[el.ID()]))
		}
	}

	if goutil.IsNil(items[id]) {
		return make([]E, 0)
	}
	return items[id].Childs()
}
