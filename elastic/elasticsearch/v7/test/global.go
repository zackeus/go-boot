package test

import (
	v7 "gitee.com/zackeus/go-boot/elastic/elasticsearch/v7"
	"os"
)

const (
	index           = "test-2024"
	username string = "elastic"
	password string = "baicfc123"
	// ca 证书
	crt = "/Users/zackeus/server-dev/es/dev/ca.crt"
)

//const (
//	index           = "cdr-2024"
//	username string = "elastic"
//	password string = "yulon123"
//	// ca 证书
//	crt = "/Users/zackeus/server-dev/es/dev/ca.crt"
//)

var (
	addresses = []string{"https://es.node1.server:9200"}
	//addresses = []string{"https://es.cluster.server:9200"}
)

func initESCLi() (*v7.Client, error) {
	caCrt, err := os.ReadFile(crt)
	if err != nil {
		return nil, err
	}

	return v7.NewClient(v7.Config{
		Addresses:            addresses,
		Username:             username,
		Password:             password,
		CACert:               caCrt,
		RetryOnStatus:        []int{502, 503, 504, 429}, // 429 TooManyRequests statuses
		DisableRetry:         false,
		EnableRetryOnTimeout: true,
		MaxRetries:           3,
	})
}
